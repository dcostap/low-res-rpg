from PIL import Image
import os

for root, subfolders, filenames in os.walk(os.path.join(".", "textures_finished")):
	print("Searching on folder: " + root)
	for file in filenames:
		if file.endswith('.png'):
			print("-> Found and processed: " + file)
			processImage(os.path.join(root, file))

def processImage(image):
	img = Image.open(image)
	img = img.convert("RGBA")
	pixdata = img.load()

	for y in xrange(img.size[1]):
		for x in xrange(img.size[0]):
			if pixdata[x, y] == (232, 227, 203, 255):
				pixdata[x, y] = (0, 0, 0, 255)