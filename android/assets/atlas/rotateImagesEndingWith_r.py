from PIL import Image
import os
 
def rotate(image_path, degrees_to_rotate, saved_location):
	image_obj = Image.open(image_path)
	rotated_image = image_obj.rotate(degrees_to_rotate)
	rotated_image.save(saved_location)
	#rotated_image.show()
 
def processFolder(foldername):
	for root, subfolders, filenames in os.walk(foldername):
		print("Searching on folder: " + root)
		for file in filenames:
			if file.endswith('_r.png'):
				print("-> Found and processed: " + file)
				degrees = [90, 180, 270]
				for i in degrees:
					name = os.path.join(root, file)
					rotate(name, i, name.replace("_r.png", "_r" + str(i) + ".png"))
		
 
if __name__ == '__main__':
	processFolder(".")