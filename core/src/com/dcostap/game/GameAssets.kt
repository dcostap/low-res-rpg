package com.dcostap.game

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.dcostap.engine_2d.engine.Assets
import com.dcostap.engine_2d.engine.Engine
import com.dcostap.engine_2d.engine.utils.font_loaders.BitmapFontLoader
import com.dcostap.engine_2d.engine.utils.font_loaders.FontSetNormal
import com.dcostap.engine_2d.engine.utils.font_loaders.FontSetSingle
import com.dcostap.engine_2d.engine.utils.font_loaders.FreeTypeFontLoader

/**
 * Created by Darius on 26/03/2018.
 */
class GameAssets : Assets() {
    val fontDefault = FontSetNormal()
    val fontPixel = FontSetSingle()

    override fun initAssetLoading() {
        val ttpFontFolder = "fonts/true_type"
        val bitmapFontFolder = "fonts/bitmap"

        // create the font loaders
        val resolutionBaseWidth = Engine.BASE_WIDTH
        addFontLoader(FreeTypeFontLoader(ttpFontFolder, "Roboto-Regular.ttf",
                fontDefault, "font1", useDensityFactor = false, useResolutionFactor = true, baseResolutionWidth = resolutionBaseWidth))
        addFontLoader(BitmapFontLoader("fonts/bitmap", "darenciusPixel.fnt", fontPixel))

        super.initAssetLoading()
    }

    override fun finishAssetLoading(): Boolean {
        if (super.finishAssetLoading()) {
            // things to do when finished loading all assets; add styles to the skin
            skin.add("default", Window.WindowStyle(fontDefault.font_small, Color.BLACK, skin.getDrawable("outline2_pixel")))
            skin.add("nonPixel", Window.WindowStyle(fontDefault.font_small, Color.BLACK, skin.getDrawable("outline2")))
            return true
        }

        return false
    }
}