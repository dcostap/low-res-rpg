package com.dcostap.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.dcostap.engine_2d.engine.Engine
import com.dcostap.game.game_screen.GameScreen

/**
 * Created by Darius on 26/03/2018.
 */
class ThisGame : Engine() {
    companion object {
        /** Changes the static variables on Engine class. Call first when the app starts */
        fun initStaticVars() {
            Engine.PPM = 16
            Engine.BASE_WIDTH = 800
            Engine.BASE_HEIGHT = 450
            Engine.DEBUG = true
            Engine.DEBUG_COLLISION_TREE_CELLS = true
            Engine.DEBUG_COLLISION_TREE_UPDATES = false
        }
    }

    lateinit var gameAssets: GameAssets
        private set

    override fun create() {
        super.create()

        gameAssets = GameAssets()
        assets = gameAssets
        assets.initAssetLoading()

        // wait for assets to load; you can move this to a loading screen!
        while (!assets.finishAssetLoading()) {

        }

        this.setScreen(GameScreen(this))
    }

    override fun render() {
        super.render()

        // debug purposes
        if (Gdx.input.isKeyPressed(Input.Keys.R))
            this.setScreen(GameScreen(this))
    }
}