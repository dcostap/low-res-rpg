package com.dcostap.game.game_screen

import com.badlogic.gdx.math.Vector2
import com.dcostap.engine_2d.engine.map.EntityTiledMap
import com.dcostap.engine_2d.engine.map.entities.Entity
import com.dcostap.engine_2d.engine.map.map_loading.EntityLoaderFromString
import com.dcostap.game.game_screen.entities.enemy.StrongEnemy
import com.dcostap.game.game_screen.entities.enemy.WeakEnemy
import com.dcostap.game.game_screen.entities.player.Player

class GameEntityLoader(private val gameScreen: GameScreen) : EntityLoaderFromString {

    override fun loadEntityFromTiledTileObject(imageName: String, position: Vector2, widthPixels: Int, heightPixels: Int, map: EntityTiledMap): Entity {
        when (imageName) {

        }
        throw RuntimeException("Tried to load a TileObject with imageName: $imageName; there is no Entity associated with it")
    }

    override fun loadEntityFromObjectName(objectName: String, position: Vector2, widthPixels: Int, heightPixels: Int, map: EntityTiledMap): Entity {
        when (objectName) {
            "player" -> return Player(position, map, gameScreen)
            "strong_enemy" -> return StrongEnemy(position, map, gameScreen)
            "weak_enemy" -> return WeakEnemy(position, map, gameScreen)
        }
        throw RuntimeException("Tried to load a Object with name: $objectName; there is no Entity associated with it")
    }
}
