package com.dcostap.game.game_screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.dcostap.engine_2d.engine.Engine
import com.dcostap.engine_2d.engine.ui.UIController
import com.dcostap.engine_2d.engine.ui.UICreator
import com.dcostap.engine_2d.engine.ui.utils.ExtendedLabel
import com.dcostap.engine_2d.engine.ui.utils.LabelButton
import com.dcostap.game.ThisGame

/**
 * Created by Darius on 28/12/2017
 */
class GameHud(uiController: UIController, val game: ThisGame, private val gameScreen: GameScreen) : UICreator(uiController) {

    lateinit var debugInfo: ExtendedLabel
    lateinit var fpsCounter: ExtendedLabel
    lateinit var entityCounter: ExtendedLabel

    lateinit var debugToggle: LabelButton

    override fun createHud() {
        debugInfo = ExtendedLabel("", game.gameAssets.fontDefault.font_small)
        uiController.centerLeftTable.add<Actor>(debugInfo)

        fpsCounter = ExtendedLabel("", game.gameAssets.fontDefault.font_medium)
        entityCounter = ExtendedLabel("", game.gameAssets.fontDefault.font_medium)
        uiController.downTable.row()
        uiController.downTable.add<Actor>(entityCounter).center().pad(20f)
        uiController.downTable.add<Actor>(fpsCounter).center()

        debugToggle = LabelButton(debugButtonText, game.gameAssets.fontDefault.font_small, skin)
        debugToggle.addListener(object : ChangeListener() {
            override fun changed(event: ChangeListener.ChangeEvent, actor: Actor) {
                Engine.DEBUG = !Engine.DEBUG
                debugToggle.setText(debugButtonText)
            }
        })

        uiController.upTable.add<Actor>(debugToggle)
    }

    private val debugButtonText: String
        get() = "Debug (" + (if (Engine.DEBUG) "yes" else "no") + ")"

    override fun update(delta: Float) {
        fpsCounter.setText("Fps: " + Integer.toString(Gdx.graphics.framesPerSecond))
        entityCounter.setText("Entities # : " + Integer.toString(gameScreen.gameMap.entityList.size))
    }

    override fun deleteHud() {
        super.deleteHud()
    }
}
