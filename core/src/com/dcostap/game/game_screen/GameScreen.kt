package com.dcostap.game.game_screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.GL20

import com.badlogic.gdx.Gdx.input
import com.badlogic.gdx.utils.viewport.ExtendViewport
import com.dcostap.engine_2d.engine.Engine
import com.dcostap.engine_2d.engine.map.EntityComparatorByYPosition
import com.dcostap.engine_2d.engine.map.EntityTiledMap
import com.dcostap.engine_2d.engine.map.map_loading.JsonMapLoader
import com.dcostap.engine_2d.engine.utils.GameDrawer
import com.dcostap.engine_2d.engine.utils.screens.BaseScreenWithHud
import com.dcostap.game.ThisGame

/**
 * Created by Darius on 28/12/2017
 */
class GameScreen(val game: ThisGame) : BaseScreenWithHud(game) {
    val gameHud: GameHud
    val gameMap: EntityTiledMap

    init {
        touchInputNotifier.registerObserver(this)

        gameHud = GameHud(uiController, game, this)
        gameHud.createHud()

        gameMap = EntityTiledMap(camera, false, 0, EntityComparatorByYPosition(), game)
        val jsonMapLoader = JsonMapLoader(gameMap, "maps/map.json",
                "maps/tilesets", "maps/object_templates", GameEntityLoader(this))
        jsonMapLoader.loadMap()
    }

    override fun create(engine: Engine) {
        super.create(engine)

        camera.zoom = 1 / 3f
    }

    override fun update(delta: Float) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            Gdx.app.exit()
        }

        gameHud.update(delta)
        gameMap.update(delta)

        // CAMERA MOVEMENT //
        if (input.isKeyPressed(Input.Keys.LEFT)) {
            camera.translate(-0.1f, 0f, 0f)
        }
        if (input.isKeyPressed(Input.Keys.RIGHT)) {
            camera.translate(0.1f, 0f, 0f)
        }
        if (input.isKeyPressed(Input.Keys.DOWN)) {
            camera.translate(0f, -0.1f, 0f)
        }
        if (input.isKeyPressed(Input.Keys.UP)) {
            camera.translate(0f, 0.1f, 0f)
        }
    }

    override fun draw(gameDrawer: GameDrawer, delta: Float) {
        // clear screen
        Gdx.gl.glClearColor(1f, 0.1f, 0.1f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        // update camera & viewport
        viewport.apply()

        // start the batch
        game.batch.projectionMatrix = camera.combined
        game.batch.begin()

        gameMap.draw(gameDrawer, delta)

        game.batch.end()

        uiController.draw(gameDrawer, delta)
    }

    override fun touchDownEvent(screenX: Float, screenY: Float, worldX: Float, worldY: Float, button: Int, pointer: Int, isJustPressed: Boolean) {

    }

    override fun touchReleasedEvent(screenX: Float, screenY: Float, worldX: Float, worldY: Float, button: Int, pointer: Int) {

    }
}
