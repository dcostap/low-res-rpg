package com.dcostap.game.game_screen.entities

import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.dcostap.engine_2d.engine.map.EntityTiledMap
import com.dcostap.engine_2d.engine.map.entities.Entity
import com.dcostap.game.game_screen.entities.fighter.Fighter
import com.dcostap.game.game_screen.entities.fighter.FighterEntityModule

/**
 * Created by Darius on 06/03/2018.
 */
class HittingBox(position: Vector2, boundingBox: Rectangle, map: EntityTiledMap, val damage: Float, val attacker: Fighter,
                 val direction: TopDownEntityModule.Direction)
    : Entity(position, boundingBox, map, false, true, false)
{
    enum class Side {
        ENEMY, ALLIED
    }

    override fun update(delta: Float) {
        super.update(delta)

        if (isKilled) return

        for (fighter in map.collisionTree.getPossibleCollidingEntities(boundingBox, true,
                FighterEntityModule.hitBoxBBname)) {
            if (fighter is Fighter) {
                if (boundingBox.overlaps((fighter as Fighter).fighterEntityModule.hitBox))
                    (fighter as Fighter).fighterEntityModule.hit(this)
            }
        }
    }

    val side: Side
        get() = attacker.side
}
