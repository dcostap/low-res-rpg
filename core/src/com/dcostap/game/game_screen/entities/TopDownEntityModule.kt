package com.dcostap.game.game_screen.entities

import com.badlogic.gdx.math.Vector2
import com.dcostap.engine_2d.engine.map.entities.CollidingEntity
import com.dcostap.engine_2d.engine.utils.Updatable
import com.dcostap.engine_2d.engine.utils.Utils

/**
 * Created by Darius on 23/02/2018.
 */
class TopDownEntityModule(var entity: CollidingEntity) : Updatable {
    var walkingSpeed = 0f
    var isWalking = false
        private set

    private val dummyVector = Vector2()
    private var pushedSpeed = 0f
    private var pushedDecrease = 0f
    private var pushedDirection: Float = 0.toFloat()

    inner class InputModule {
        var inputDown: Boolean = false
        var inputUp: Boolean = false
        var inputLeft: Boolean = false
        var inputRight: Boolean = false

        fun resetInputBooleans() {
            inputUp = false
            inputDown = inputUp
            inputRight = inputDown
            inputLeft = inputRight
        }

        fun anyMovementIsTrue(): Boolean {
            return inputDown || inputUp || inputLeft || inputRight
        }
    }

    var inputModule = InputModule()

    enum class Direction {
        UP, DOWN, LEFT, RIGHT;

        fun toDegrees(): Int {
            return when (this) {
                UP -> 90
                RIGHT -> 0
                LEFT -> 180
                DOWN -> 270
            }
        }

        val contrary: Direction
            get() {
                return when (this) {
                    DOWN -> UP
                    LEFT -> RIGHT
                    RIGHT -> LEFT
                    UP -> DOWN
                }
            }

        companion object {
            fun getClosestDirectionFromDegrees(directionDegrees: Float): Direction {
                var minDifference = java.lang.Float.MAX_VALUE

                var chosenDirection: TopDownEntityModule.Direction = TopDownEntityModule.Direction.LEFT

                for (dir in TopDownEntityModule.Direction.values()) {
                    val difference = Utils.getAngleDifferenceNotSigned(directionDegrees, dir.toDegrees().toFloat())

                    if (difference < minDifference) {
                        minDifference = difference
                        chosenDirection = dir
                    }
                }

                return chosenDirection
            }
        }
    }

    /** restricted to the 4 axis, so on diagonal movement one of them is picked  */
    var movingDirection: Direction? = Direction.DOWN
    var secondaryFacingDirection: Direction? = null
    var changedMovingDirection = false

    fun pushEntity(initialSpeed: Float, decrease: Float, direction: Float) {
        pushedSpeed = initialSpeed
        pushedDecrease = decrease
        pushedDirection = direction
    }

    /** Based on the input flags on the inputModule, modifies speed. Flag [.changedMovingDirection] will
     * be set to true if movingDirection changed, that is, the entity moved in a different direction. If [.walkingSpeed]
     * is 0 the flag will always be false  */
    override fun update(delta: Float) {
        entity.setSpeed(0f, 0f)
        changedMovingDirection = false
        secondaryFacingDirection = null

        if (pushedSpeed > 0) {
            val xAdd = Utils.getXFromDirectionMovement(pushedSpeed, pushedDirection)
            val yAdd = Utils.getYFromDirectionMovement(pushedSpeed, pushedDirection)
            entity.speed.add(xAdd, yAdd)

            pushedSpeed -= pushedDecrease * delta
        }

        walkingUpdate(delta)
    }

    private fun walkingUpdate(delta: Float) {
        if (walkingSpeed == 0f) return

        dummyVector.set(0f, 0f)

        // just started walking
        if (inputModule.anyMovementIsTrue() && !isWalking) movingDirection = null

        if (inputModule.anyMovementIsTrue() && (movingDirection == Direction.UP && !inputModule.inputUp
                || movingDirection == Direction.DOWN && !inputModule.inputDown
                || movingDirection == Direction.LEFT && !inputModule.inputLeft
                || movingDirection == Direction.RIGHT && !inputModule.inputRight)) {
            movingDirection = null
        }

        val oldMovingDirection = movingDirection

        if (movingDirection != Direction.RIGHT && movingDirection != Direction.LEFT && inputModule.inputUp) {
            movingDirection = Direction.UP
            dummyVector.y += walkingSpeed
        } else if (movingDirection != Direction.RIGHT && movingDirection != Direction.LEFT && inputModule.inputDown) {
            movingDirection = Direction.DOWN
            dummyVector.y -= walkingSpeed
        } else if (movingDirection != Direction.UP && movingDirection != Direction.DOWN && inputModule.inputLeft) {
            movingDirection = Direction.LEFT
            dummyVector.x -= walkingSpeed
        } else if (movingDirection != Direction.UP && movingDirection != Direction.DOWN && inputModule.inputRight) {
            movingDirection = Direction.RIGHT
            dummyVector.x += walkingSpeed
        }

        if (movingDirection == Direction.UP || movingDirection == Direction.DOWN) {
            if (inputModule.inputLeft) {
                secondaryFacingDirection = Direction.LEFT
            } else if (inputModule.inputRight) {
                secondaryFacingDirection = Direction.RIGHT
            }
        }

        if (movingDirection == Direction.LEFT || movingDirection == Direction.RIGHT) {
            if (inputModule.inputDown) {
                secondaryFacingDirection = Direction.DOWN
            } else if (inputModule.inputUp) {
                secondaryFacingDirection = Direction.UP
            }
        }

        if (oldMovingDirection != movingDirection) {
            changedMovingDirection = true
        }

        // diagonal movement
        if (secondaryFacingDirection != null) {
            val reduction = 1.3f

            if (secondaryFacingDirection == Direction.LEFT || secondaryFacingDirection == Direction.RIGHT) {
                dummyVector.y /= reduction
                dummyVector.x = if (secondaryFacingDirection == Direction.LEFT) -walkingSpeed / reduction else walkingSpeed / reduction
            } else {
                dummyVector.x /= reduction
                dummyVector.y = if (secondaryFacingDirection == Direction.DOWN) -walkingSpeed / reduction else walkingSpeed / reduction
            }
        }

        isWalking = dummyVector.x != 0f || dummyVector.y != 0f
        entity.speed.add(dummyVector)
    }
}
