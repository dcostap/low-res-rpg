package com.dcostap.game.game_screen.entities.enemy

import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.dcostap.engine_2d.engine.map.EntityTiledMap
import com.dcostap.engine_2d.engine.map.entities.CollidingEntity
import com.dcostap.engine_2d.engine.utils.GameDrawer
import com.dcostap.engine_2d.engine.utils.Utils
import com.dcostap.engine_2d.engine.utils.finite_state_machine.StateMachine
import com.dcostap.game.game_screen.GameScreen
import com.dcostap.game.game_screen.entities.HittingBox
import com.dcostap.game.game_screen.entities.TopDownEntityModule
import com.dcostap.game.game_screen.entities.fighter.*
import com.dcostap.game.game_screen.entities.player.PlayerAnimation

/**
 * Created by Darius on 11/02/2018.
 */
open class BaseEnemy(position: Vector2, map: EntityTiledMap, internal var gameScreen: GameScreen)
    : FighterBase(position, Utils.transformRectWithPixelsToGameUnits(Rectangle(-7f, -7f, 13f, 13f)),
        map, true, null), Fighter
{
    override val fighterAnimation = PlayerAnimation(this, gameScreen.game.assets)

    init {
        setup()
    }

    internal var fighterAI = FighterAI(this)

    override fun update(delta: Float) {
        super.update(delta)

        fighterAI.update(delta)

        fighterEntityModule.update(delta)
        stateMachine.update(delta)
    }

    override val side: HittingBox.Side
        get() = HittingBox.Side.ENEMY

    //region draw stuff
    override fun draw(gameDrawer: GameDrawer, delta: Float) {
        fighterAnimation.draw(gameDrawer, delta)

        fighterEntityModule.drawDebugFighterInfo(gameDrawer, delta)

        gameDrawer.drawTextInUI(x + 1.5f, y + 0.3f,
                fighterAI.stateMachine.currentState!!.javaClass.simpleName)

        super.draw(gameDrawer, delta)
    }

    //endregion
}
