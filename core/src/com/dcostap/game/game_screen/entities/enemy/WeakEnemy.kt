package com.dcostap.game.game_screen.entities.enemy

import com.badlogic.gdx.math.Vector2
import com.dcostap.engine_2d.engine.map.EntityTiledMap
import com.dcostap.game.game_screen.GameScreen

/**
 * Created by Darius on 20/03/2018.
 */
class WeakEnemy(position: Vector2, map: EntityTiledMap, gameScreen: GameScreen) : BaseEnemy(position, map, gameScreen) {
    init {
        fighterEntityModule.config.maximumStamina = 60f
        fighterEntityModule.currentStamina = 60f

        fighterEntityModule.config.maximumHealth = 60f
        fighterEntityModule.currentHealth = 60f
    }
}
