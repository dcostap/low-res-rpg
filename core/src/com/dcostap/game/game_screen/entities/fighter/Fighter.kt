package com.dcostap.game.game_screen.entities.fighter

import com.dcostap.engine_2d.engine.map.entities.CollidingEntity
import com.dcostap.engine_2d.engine.utils.finite_state_machine.StateMachine
import com.dcostap.game.game_screen.entities.HittingBox
import com.dcostap.game.game_screen.entities.TopDownEntityModule

/**
 * Created by Darius on 06/03/2018.
 */
interface Fighter {
    val topDownEntityModule: TopDownEntityModule
    val side: HittingBox.Side
    val fighterEntityModule: FighterEntityModule
    val collidingEntity: CollidingEntity

    val fighterAnimation: FighterAnimation

    val stateMachine: StateMachine<Fighter>
    val fighterStateWalking: FighterStateWalking
    val fighterStateIdle: FighterStateIdle
    val fighterStateDash: FighterStateDash
    val fighterStateDefending: FighterStateDefending
    val fighterStateAttacking: FighterStateAttacking
    val fighterStateStagger: FighterStateStagger
}
