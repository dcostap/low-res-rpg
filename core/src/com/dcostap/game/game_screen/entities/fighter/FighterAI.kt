package com.dcostap.game.game_screen.entities.fighter

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Pools
import com.dcostap.engine_2d.engine.utils.*
import com.dcostap.engine_2d.engine.utils.finite_state_machine.State
import com.dcostap.engine_2d.engine.utils.finite_state_machine.StateMachine
import com.dcostap.game.game_screen.entities.TopDownEntityModule

/**
 * Created by Darius on 14/03/2018.
 */
class FighterAI(var fighter: Fighter) : Updatable {
    var enemy: Fighter? = null
    var stateMachine: StateMachine<Fighter>
    private val fightIdle = FightIdle()
    private val lightAttack = LightAttack()
    private val hardAttack = HardAttack()
    private val idle = Idle()
    private val reactToEnemyAttack = ReactToEnemyAttack()

    var enemySearchMaxDistance = 4f
    var fightMovementStrat = FightMovementStrat.NORMAL

    private var aiInput: TopDownEntityModule.InputModule

    init {
        stateMachine = StateMachine(fighter, idle, null)
        aiInput = TopDownEntityModule(fighter.collidingEntity).InputModule()
    }

    override fun update(delta: Float) {
        fighter.topDownEntityModule.inputModule.resetInputBooleans()
        fighter.fighterEntityModule.inputModule.resetInputBooleans()

        if (stopMovementTimer.isTimerOn) {
            if (stopMovementTimer.tick(delta)) {
                stopMovement = false
            }
        }

        updateMovement(delta)
        stateMachine.update(delta)
    }

    private var positionObjective = Vector2()
    private val changeDirectionTimer = Timer(0.45f)
    private var stopMovement = false
    private val stopMovementTimer = Timer(1f, false, true)

    private fun stopMovementSomeTime(time: Float) {
        stopMovementTimer.resetTimer()
        stopMovementTimer.timeLimit = time
        stopMovementTimer.turnTimerOn()
        stopMovement = true
    }

    private fun updateMovement(delta: Float) {
        fighter.topDownEntityModule.inputModule.inputDown = aiInput.inputDown
        fighter.topDownEntityModule.inputModule.inputLeft = aiInput.inputLeft
        fighter.topDownEntityModule.inputModule.inputRight = aiInput.inputRight
        fighter.topDownEntityModule.inputModule.inputUp = aiInput.inputUp

        if (!changeDirectionTimer.tick(delta)) return
        aiInput.resetInputBooleans()

        if (stopMovement) return
        val position = fighter.collidingEntity.position
        val margin = 0.15f
        if (position.x < positionObjective.x - margin) {
            aiInput.inputRight = true
        }
        if (position.y < positionObjective.y - margin) {
            aiInput.inputUp = true
        }
        if (position.x > positionObjective.x + margin) {
            aiInput.inputLeft = true
        }
        if (position.y > positionObjective.y + margin) {
            aiInput.inputDown = true
        }
    }

    private fun updateFightingMovementObjective() {
        var distanceToFoe = 0f

        when (fightMovementStrat) {
            FighterAI.FightMovementStrat.FLEE -> distanceToFoe = enemy!!.fighterEntityModule.config.hardAttackHitWidth +
                    enemy!!.fighterEntityModule.config.hardAttackExtraWidth + 0.14f
            FighterAI.FightMovementStrat.ENGAGE -> distanceToFoe = fighter.fighterEntityModule.config.lightAttackHitWidth / 2f
            FighterAI.FightMovementStrat.NORMAL -> distanceToFoe = enemy!!.fighterEntityModule.config.lightAttackHitWidth + 0.2f
        }

        var center = positionObjective
        center = FighterEntityModule.getOuterCenter(enemy!!, fighter.fighterEntityModule.facingDirection.contrary, center)

        when (fighter.fighterEntityModule.facingDirection) {
            TopDownEntityModule.Direction.UP -> center.y -= distanceToFoe
            TopDownEntityModule.Direction.RIGHT -> center.x -= distanceToFoe
            TopDownEntityModule.Direction.LEFT -> center.x += distanceToFoe
            TopDownEntityModule.Direction.DOWN -> center.y += distanceToFoe
        }

        positionObjective = center
    }

    private inner class Idle : State<Fighter> {
        internal var searchTimer = Timer(0.2f)
        override fun enter(being: Fighter) {
            searchTimer.resetTimer()
            fighter.fighterEntityModule.lockDirectionToEnemy(null)
        }

        override fun execute(being: Fighter, delta: Float) {
            if (enemy != null) {
                stateMachine.changeState(fightIdle)
            } else {
                if (searchTimer.tick(delta)) {
                    var minDistance = java.lang.Float.MAX_VALUE
                    var chosenEnemy: Fighter? = null
                    for (entity in fighter.collidingEntity.map.collisionTree.getClosestEntities(fighter.collidingEntity.boundingBox, enemySearchMaxDistance.toInt())) {
                        if (entity !is Fighter) continue
                        if (entity.side === fighter.side) continue

                        val distance = Utils.getDistanceBetweenPoints(entity.position, fighter.collidingEntity.position)
                        if (distance < minDistance) {
                            chosenEnemy = entity
                            minDistance = distance
                        }
                    }

                    if (chosenEnemy != null) {
                        enemy = chosenEnemy
                        fighter.fighterEntityModule.lockDirectionToEnemy(enemy)
                    }
                }
            }
        }

        override fun exit(being: Fighter) {

        }
    }

    enum class FightMovementStrat {
        FLEE, NORMAL, ENGAGE
    }

    private inner class FightIdle : State<Fighter> {
        internal var shouldLightAttackTimer = Timer(0.4f)
        internal var shouldHardAttackTimer = Timer(0.4f)
        internal var shouldChangeMoveStratTimer = Timer(1.3f)
        internal var reactionTimer = Timer(0.03f)
        internal var shield: Boolean = false
        internal var shieldTimer = Timer(0.15f)
        internal var updateMovementTimer = Timer(0.25f)

        override fun enter(being: Fighter) {
            shouldLightAttackTimer.elapsed = shouldLightAttackTimer.timeLimit // immediate tick
            shouldHardAttackTimer.elapsed = shouldHardAttackTimer.timeLimit/ 2f
            reactionTimer.resetTimer()
            shield = false
            shieldTimer.resetTimer()
            updateMovementTimer.resetTimer()
            shouldChangeMoveStratTimer.elapsed = shouldLightAttackTimer.timeLimit
        }

        override fun execute(being: Fighter, delta: Float) {
            if (enemy == null) {
                stateMachine.changeState(idle)
                return
            }

            if (fighter.stateMachine.currentState === fighter.fighterStateStagger) {
                return
            }

            if (updateMovementTimer.tick(delta)) {
                updateFightingMovementObjective()
            }
            if (shouldChangeMoveStratTimer.tick(delta)) {
                val previousStrat = fightMovementStrat
                if (Utils.getChanceToBeTrue(shouldLightAttack(fighter, enemy) + 15)) {
                    fightMovementStrat = FightMovementStrat.ENGAGE
                } else {
                    if (Utils.getChanceToBeTrue(shouldLightAttack(enemy, fighter) - 10)) {
                        fightMovementStrat = FightMovementStrat.FLEE
                    } else {
                        fightMovementStrat = FightMovementStrat.NORMAL
                    }
                }

                if (fightMovementStrat != previousStrat) {
                    stopMovementSomeTime(Utils.getRandomFloatInsideRange(0.1f, 0.43f, false))

                    println("Changed fight strat to: " + fightMovementStrat)
                }

                if (being.fighterEntityModule.gameDrawer != null)
                    being.fighterEntityModule.gameDrawer!!.drawFlashingTextInUI(fightMovementStrat.toString(),
                            being.collidingEntity.position.x,
                            being.collidingEntity.position.y, FlashingThing(Color.BLACK, 0.6f, 1f))
            }

            being.fighterEntityModule.gameDrawer.whenNotNull {
                it.drawLineInUI(being.collidingEntity.position.x,
                        being.collidingEntity.position.y, positionObjective.x, positionObjective.y, true)
            }

            // random shielding
            if (shieldTimer.tick(delta)) {
                if (!shield) {
                    // in range of light attack ?
                    if (fighterHitsFighter(enemy!!, fighter, false, 0.15f)) {
                        // if stamina is high
                        if (Utils.getChanceToBeTrue(Utils.mapToRange(fighter.fighterEntityModule.currentStamina,
                                0f, fighter.fighterEntityModule.config.maximumStamina,
                                0f, 8f))) {
                            shield = true
                        }
                    } else { // not in range of light attack, so shielding serves no practical purpose
                        var chances = 0f
                        // if movement is stopped
                        chances += (if (stopMovement) 30 else 0).toFloat()

                        // stamina must be somewhat high
                        chances -= Utils.mapToRange(fighter.fighterEntityModule.currentStamina,
                                fighter.fighterEntityModule.config.maximumStamina / 2f,
                                fighter.fighterEntityModule.config.maximumStamina,
                                50f, 0f)

                        // random variation
                        chances += +Utils.getRandomFloatInsideRange(0f, 10f, true)

                        if (Utils.getChanceToBeTrue(chances)) {
                            shield = true
                        }
                    }
                } else {
                    // if stamina is not high, stop
                    var chances = Utils.mapToRange(fighter.fighterEntityModule.currentStamina,
                            fighter.fighterEntityModule.config.maximumStamina / 2.3f,
                            fighter.fighterEntityModule.config.maximumStamina,
                            80f, 5f)

                    // random variation
                    chances += +Utils.getRandomFloatInsideRange(0f, 10f, true)

                    if (Utils.getChanceToBeTrue(chances)) {
                        shield = false
                    }
                }
            }

            if (shield) fighter.fighterEntityModule.inputModule.inputDefend = true

            if (enemy!!.stateMachine.currentState === enemy!!.fighterStateAttacking) {
                if (reactionTimer.tick(delta)) {
                    val reactionChances = 50f

                    if (Utils.getChanceToBeTrue(reactionChances)) {
                        stateMachine.changeState(reactToEnemyAttack)
                        return
                    }
                }
            } else {
                reactionTimer.resetTimer()
            }

            if (shouldHardAttackTimer.tick(delta)) {
                if (fighterHitsFighter(fighter, enemy!!, true, 0.15f)) {
                    val shouldAttack = shouldHardAttack(fighter, enemy!!)

                    fighter.fighterEntityModule.gameDrawer.whenNotNull {
                        it.drawFlashingTextInUI(shouldAttack.toString() + "%",
                                fighter.collidingEntity.x + 1.4f, fighter.collidingEntity.y - 0.23f,
                                FlashingThing(Color.RED, 0.15f, 0.65f))
                    }

                    if (Utils.getChanceToBeTrue(shouldAttack)) {
                        stateMachine.changeState(hardAttack)
                    }
                }
            }

            if (shouldLightAttackTimer.tick(delta)) {
                if (fighterHitsFighter(fighter, enemy!!, false, 0.1f)) {

                    fighter.fighterEntityModule.gameDrawer.whenNotNull {
                        it.drawFlashingTextInUI("ai decision!",
                                fighter.collidingEntity.x + 1f, fighter.collidingEntity.y,
                                FlashingThing(Color.BLACK, 0.15f, 0.65f))
                    }

                    val shouldAttack = shouldLightAttack(fighter, enemy)

                    fighter.fighterEntityModule.gameDrawer.whenNotNull {
                        it.drawFlashingTextInUI(shouldAttack.toString() + "%",
                                fighter.collidingEntity.x + 1f, fighter.collidingEntity.y - 0.23f,
                                FlashingThing(Color.BLACK, 0.15f, 0.65f))
                    }

                    if (Utils.getChanceToBeTrue(shouldAttack)) {
                        stateMachine.changeState(lightAttack)
                    }
                }
            }
        }

        private fun shouldLightAttack(fighter: Fighter?, enemy: Fighter?): Float {
            var shouldAttack = 0f
            // attack if enemy has low stamina
            shouldAttack += Utils.mapToRange(enemy!!.fighterEntityModule.currentStamina, 0f,
                    enemy.fighterEntityModule.config.maximumStamina / 1.2f, 65f, 0f)

            // if you have stamina advantage from a light attack, attack
            val enemyStaminaLoss = FighterEntityModule.getStaminaLossFromDefendingAttack(
                    fighter!!.fighterEntityModule.config.lightAttackStrength,
                    enemy.fighterEntityModule.config.shieldStrength)
            val staminaAdvantageFromLightAttack = enemyStaminaLoss - fighter.fighterEntityModule.config.lightAttackStaminaCost
            shouldAttack += Utils.mapToRange(staminaAdvantageFromLightAttack,
                    0f, enemy.fighterEntityModule.config.maximumStamina,
                    0f, 75f)

            // if enemy loses all stamina from a light attack, attack
            if (enemyStaminaLoss > enemy.fighterEntityModule.currentStamina) {
                shouldAttack += 25f
            }

            // if enemy is staggered, attack
            shouldAttack += (if (enemy.stateMachine.currentState === enemy.fighterStateStagger) 90 else 0).toFloat()

            // don't attack if enemy is defending
            shouldAttack += (if (enemy.stateMachine.currentState === enemy.fighterStateDefending) 0 else 38).toFloat()

            // don't attack if enemy is attacking, attack if enemy is doing hard attack
            if (enemy.stateMachine.currentState === enemy.fighterStateAttacking) {
                if (enemy.fighterStateAttacking.isHardAttack) {
                    shouldAttack += 45f
                } else {
                    shouldAttack -= 100f
                }
            }

            // attack if enemy health is low compared to light attack strength (you can kill enemy from 1 hit)
            shouldAttack += Utils.mapToRange(enemy.fighterEntityModule.currentHealth, 0f,
                    fighter.fighterEntityModule.config.lightAttackStrength * 4.2f, 85f, 0f)

            // don't attack if stamina is too low
            shouldAttack -= Utils.mapToRange(fighter.fighterEntityModule.currentStamina,
                    fighter.fighterEntityModule.config.lightAttackStaminaCost / 1.2f,
                    fighter.fighterEntityModule.config.lightAttackStaminaCost * 1.3f, 90f, 0f)

            // random
            shouldAttack += Utils.getRandomFloatInsideRange(0f, 10f, true)

            println("shouldLightAttack: " + shouldAttack)
            return shouldAttack
        }

        private fun shouldHardAttack(fighter: Fighter, enemy: Fighter): Float {
            var shouldAttack = 0f

            // whether you are at enough distance / not on reach of an enemy light attack
            // (that would mean he can counter your hard attack easily and hit you)
            // minDistance used as a rough estimation; assures it is at least somewhat far away
            // real test is whether the enemy's light attack hitBox hits the fighter
            val distanceFromEnemy = Utils.getDistanceBetweenPoints(enemy.collidingEntity.position,
                    fighter.collidingEntity.position)
            val minDistance = enemy.fighterEntityModule.config.lightAttackHitWidth / 2f

            if (distanceFromEnemy < minDistance || fighterHitsFighter(enemy, fighter, false, 0.15f)) {
                return 0f
            }

            // if you don't have stamina advantage from a light attack, attack
            val enemyStaminaLoss = FighterEntityModule.getStaminaLossFromDefendingAttack(
                    fighter.fighterEntityModule.config.lightAttackStrength,
                    enemy.fighterEntityModule.config.shieldStrength)
            val staminaAdvantageFromLightAttack = enemyStaminaLoss - fighter.fighterEntityModule.config.lightAttackStaminaCost
            shouldAttack += Utils.mapToRange(staminaAdvantageFromLightAttack,
                    0f, enemy.fighterEntityModule.config.maximumStamina,
                    13f, 0f)

            // if hard attack drains all stamina from enemy, attack
            val enemyStaminaLossFromHardAttack = FighterEntityModule.getStaminaLossFromDefendingAttack(
                    fighter.fighterEntityModule.config.hardAttackStrength, enemy.fighterEntityModule.config.shieldStrength)
            shouldAttack += (if (enemy.fighterEntityModule.currentStamina < enemyStaminaLossFromHardAttack) 10 else -5).toFloat()

            // if enemy is staggered, attack
            shouldAttack += (if (enemy.stateMachine.currentState === enemy.fighterStateStagger) 5 else 0).toFloat()

            // attack if enemy is light-attacking
            if (enemy.stateMachine.currentState === enemy.fighterStateAttacking) {
                if (enemy.fighterStateAttacking.isHardAttack) {
                    shouldAttack -= 10f
                } else {
                    shouldAttack += 50f
                }
            }

            // attack if enemy health is low compared to light attack strength (you can kill enemy from 1 hit)
            shouldAttack += Utils.mapToRange(enemy.fighterEntityModule.currentHealth, 0f,
                    fighter.fighterEntityModule.config.lightAttackStrength * 4.2f, 85f, 0f)

            // don't attack if stamina is too low
            val totalStaminaCost = fighter.fighterEntityModule.config.lightAttackStaminaCost + fighter.fighterEntityModule.config.hardAttackExtraStaminaCost
            shouldAttack -= Utils.mapToRange(fighter.fighterEntityModule.currentStamina,
                    totalStaminaCost / 1.01f, totalStaminaCost * 1.1f, 80f, 0f)

            println("shouldHardAttack: " + shouldAttack)
            return shouldAttack
        }

        private fun fighterHitsFighter(attacker: Fighter, attacked: Fighter, hardAttack: Boolean, rectangleGrowth: Float): Boolean {
            var hittingBox = Pools.obtain(Rectangle::class.java)
            hittingBox = FighterEntityModule.getHittingBoxRectangle(attacker,
                    attacker.fighterEntityModule.facingDirection, hardAttack, hittingBox)

            if (hardAttack) {
                // add the extra size for hard attacks
                when (attacker.fighterEntityModule.facingDirection) {
                    TopDownEntityModule.Direction.UP -> hittingBox.height += attacker.fighterEntityModule.config.hardAttackExtraWidth
                    TopDownEntityModule.Direction.DOWN -> {
                        hittingBox.height += attacker.fighterEntityModule.config.hardAttackExtraWidth
                        hittingBox.y -= attacker.fighterEntityModule.config.hardAttackExtraWidth
                    }
                    TopDownEntityModule.Direction.RIGHT -> hittingBox.width += attacker.fighterEntityModule.config.hardAttackExtraWidth
                    TopDownEntityModule.Direction.LEFT -> {
                        hittingBox.width += attacker.fighterEntityModule.config.hardAttackExtraWidth
                        hittingBox.x -= attacker.fighterEntityModule.config.hardAttackExtraWidth
                    }
                }
            }

            //            fighter.getFighterEntityModule().getGameDrawer().drawFlashingRectangleInUI(new Rectangle(hittingBox),
            //                    new FlashingThing(Color.FIREBRICK, 0.4f, 0.5f));

            Utils.growRectangle(hittingBox, rectangleGrowth)
            val result = hittingBox.overlaps(attacked.collidingEntity.boundingBox)

            Pools.free(hittingBox)
            return result
        }

        override fun exit(being: Fighter) {

        }
    }

    private inner class LightAttack : State<Fighter> {
        override fun enter(being: Fighter) {
            maximumTimePressingAttack.resetTimer()
            maximumTimePressingAttack.turnTimerOn()
        }

        internal var maximumTimePressingAttack = Timer(0.2f, true, true)
        override fun execute(being: Fighter, delta: Float) {
            maximumTimePressingAttack.tick(delta)
            if (maximumTimePressingAttack.isTimerOn && being.stateMachine.currentState !== being.fighterStateAttacking) {
                fighter.fighterEntityModule.inputModule.inputAttack = true
            } else {
                if (being.stateMachine.currentState !== being.fighterStateAttacking) {
                    stateMachine.changeState(fightIdle)
                }
            }
        }

        override fun exit(being: Fighter) {

        }
    }

    private inner class HardAttack : State<Fighter> {
        override fun enter(being: Fighter) {
            maximumTimePressingAttack.resetTimer()
            maximumTimePressingAttack.turnTimerOn()
        }

        internal var maximumTimePressingAttack = Timer(0.15f, true, true)
        override fun execute(being: Fighter, delta: Float) {
            maximumTimePressingAttack.tick(delta)
            if (maximumTimePressingAttack.isTimerOn && being.stateMachine.currentState !== being.fighterStateAttacking) {
                fighter.fighterEntityModule.inputModule.inputForceHardAttack = true
            } else {
                if (being.stateMachine.currentState !== being.fighterStateAttacking) {
                    stateMachine.changeState(fightIdle)
                }
            }
        }

        override fun exit(being: Fighter) {

        }
    }

    private enum class Reaction {
        DASH, SHIELD, MOVE
    }

    private inner class ReactToEnemyAttack : State<Fighter> {
        lateinit internal var reaction: Reaction
        internal var notDangerousTime: Float = 0.toFloat()
        internal var exitRetardTimer = Timer(1f)

        override fun enter(being: Fighter) {
            exitRetardTimer.timeLimit = Utils.getRandomFloatInsideRange(0.1f, 0.25f, false)
            exitRetardTimer.resetTimer()

            reaction = Reaction.MOVE

            if (being.fighterStateAttacking.isHardAttack) {
                if (Utils.getChanceToBeTrue(80f))
                    reaction = Reaction.DASH
                else if (Utils.getChanceToBeTrue(80f)) reaction = Reaction.SHIELD
            } else {
                if (fighter.fighterEntityModule.currentStamina <= FighterEntityModule.getStaminaLossFromDefendingAttack(enemy!!.fighterEntityModule.config.lightAttackStrength,
                        fighter.fighterEntityModule.config.shieldStrength)) {
                    if (Utils.getChanceToBeTrue(75f)) {
                        reaction = Reaction.DASH
                    } else if (Utils.getChanceToBeTrue(40f)) {
                        reaction = Reaction.SHIELD // bad choice
                    }
                } else {
                    if (Utils.getChanceToBeTrue(75f)) {
                        reaction = Reaction.SHIELD
                    } else if (Utils.getChanceToBeTrue(55f)) {
                        reaction = Reaction.DASH // bad choice
                    }
                }
            }

            val enemyAttackingState = enemy!!.fighterStateAttacking
            if (enemyAttackingState.isHardAttack) {
                notDangerousTime = enemyAttackingState.hardAttackState2createHitTime + enemyAttackingState.hitSurvivalTime
            } else if (enemyAttackingState.isComboLightAttack) {
                notDangerousTime = enemyAttackingState.comboState2createHitTime + enemyAttackingState.hitSurvivalTime
            } else {
                notDangerousTime = enemyAttackingState.state2createHitTime + enemyAttackingState.hitSurvivalTime
            }

            fighter.fighterEntityModule.gameDrawer.whenNotNull {
                it.drawFlashingTextInUI(reaction.toString(),
                        fighter.collidingEntity.x + 1.3f, fighter.collidingEntity.y - 0.23f,
                        FlashingThing(Color.BLACK, 0.5f, 0.65f))
            }
        }

        override fun execute(being: Fighter, delta: Float) {
            if (enemy!!.fighterStateAttacking.animationTimerElapsed > notDangerousTime || enemy!!.stateMachine.currentState !== enemy!!.fighterStateAttacking) {
                if (exitRetardTimer.tick(delta)) {
                    stateMachine.changeState(fightIdle)
                }
            }

            when (reaction) {
                FighterAI.Reaction.DASH -> if (being.stateMachine.currentState !== being.fighterStateDash) {
                    val direction = Utils.getAngleBetweenPoints(enemy!!.collidingEntity.position,
                            fighter.collidingEntity.position)

                    being.topDownEntityModule.movingDirection = TopDownEntityModule.Direction.getClosestDirectionFromDegrees(direction)
                    being.fighterEntityModule.inputModule.inputDash = true
                }
                FighterAI.Reaction.SHIELD -> {
                    being.fighterEntityModule.inputModule.inputDefend = true
                    fightMovementStrat = FightMovementStrat.NORMAL
                }
                FighterAI.Reaction.MOVE -> fightMovementStrat = FightMovementStrat.FLEE
            }
        }

        override fun exit(being: Fighter) {

        }
    }
}
