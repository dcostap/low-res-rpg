package com.dcostap.game.game_screen.entities.fighter

import com.dcostap.engine_2d.engine.utils.Drawable

/**
 * Created by Darius on 17/03/2018.
 */
interface FighterAnimation : Drawable {
    fun idleStart()
    fun idleExecute()
    fun walkingStart()
    fun walkingExecute()
    fun attackStart(stateAttacking: FighterStateAttacking)
    fun attackExecute(stateAttacking: FighterStateAttacking)
    fun defendingStart()
    fun defendingExecute()
}
