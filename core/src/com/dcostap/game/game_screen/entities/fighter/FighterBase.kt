package com.dcostap.game.game_screen.entities.fighter

import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.dcostap.engine_2d.engine.map.EntityTiledMap
import com.dcostap.engine_2d.engine.map.entities.CollidingEntity
import com.dcostap.engine_2d.engine.utils.GameDrawer
import com.dcostap.engine_2d.engine.utils.finite_state_machine.StateMachine
import com.dcostap.game.game_screen.entities.TopDownEntityModule

/**
 * Created by Darius on 28/03/2018.
 */
abstract class FighterBase(position: Vector2, boundingBox: Rectangle, map: EntityTiledMap, isSolid: Boolean, gameDrawer: GameDrawer?)
    : CollidingEntity(position, boundingBox, map, isSolid, gameDrawer), Fighter {
    val fighterBaseStates = FighterBaseStates()
    override val fighterStateWalking: FighterStateWalking = fighterBaseStates.fighterStateWalking
    override val fighterStateIdle: FighterStateIdle = fighterBaseStates.fighterStateIdle
    override val fighterStateDash: FighterStateDash = fighterBaseStates.fighterStateDash
    override val fighterStateDefending: FighterStateDefending = fighterBaseStates.fighterStateDefending
    override val fighterStateAttacking: FighterStateAttacking = fighterBaseStates.fighterStateAttacking
    override val fighterStateStagger: FighterStateStagger = fighterBaseStates.fighterStateStagger

    override val collidingEntity: CollidingEntity = this

    override lateinit var stateMachine: StateMachine<Fighter>
    override lateinit var topDownEntityModule: TopDownEntityModule
    override lateinit var fighterEntityModule: FighterEntityModule

    fun setup() {
        fighterEntityModule = FighterEntityModule(this, gameDrawer)
        topDownEntityModule = TopDownEntityModule(this)
        stateMachine = StateMachine(this, fighterBaseStates.fighterStateIdle, null)
    }
}