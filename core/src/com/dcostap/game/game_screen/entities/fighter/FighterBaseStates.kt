package com.dcostap.game.game_screen.entities.fighter

/**
 * Created by Darius on 17/03/2018.
 */
class FighterBaseStates {
    var fighterStateWalking = FighterStateWalking()
    var fighterStateIdle = FighterStateIdle()
    var fighterStateAttacking = FighterStateAttacking()
    var fighterStateDefending = FighterStateDefending()
    var fighterStateDash = FighterStateDash()
    var fighterStateStagger = FighterStateStagger()
}
