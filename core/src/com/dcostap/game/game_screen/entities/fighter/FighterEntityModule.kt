package com.dcostap.game.game_screen.entities.fighter

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Pools
import com.dcostap.engine_2d.engine.Engine
import com.dcostap.engine_2d.engine.utils.*
import com.dcostap.game.game_screen.entities.HittingBox
import com.dcostap.game.game_screen.entities.TopDownEntityModule

/**
 * Created by Darius on 23/02/2018.
 * @constructor the default hitBox has size 0
 */
class FighterEntityModule(private val fighter: Fighter, val gameDrawer: GameDrawer? = null) {
    var blocked = false
    var invincible = false

    var inputModule = InputModule()
    private val hitBy = Array<HittingBox>()

    var lockedDirection: Fighter? = null
    var facingDirection = TopDownEntityModule.Direction.DOWN
    var changedFacingDirection = false
    private var oldFacingDirection: TopDownEntityModule.Direction? = null

    private var lockStaminaRegeneration: Float = 0f

    private val attackMaxDelayForCombo = 0.1f
    private var attackForComboDelay = 0f

    var currentHealth: Float = 0f
    var currentStamina: Float = 0f

    private val gotHitStreakLimit = 1f
    private var gotHitStreakTimer = 0f
    private var gotHitStaggerActualTime = 0f

    var config: Configuration

    init {
        config = Configuration()
        currentHealth = config.maximumHealth
        currentStamina = config.maximumStamina

        fighter.collidingEntity.addBoundingBox(hitBoxBBname, Rectangle())
    }

    val hitBox: Rectangle
        get() = fighter.collidingEntity.getBoundingBox(hitBoxBBname)

    inner class Configuration {
        var maximumHealth = 50f
        var maximumStamina = 50f

        var lightAttackStrength = 20f

        var hardAttackStrength = 70f
        /** shielding attacks of same strength wastes 5 stamina  */
        var shieldStrength = 15f

        var lightAttackStaminaCost = 15f
        /** note that hard attack stamina cost is added to the light attack cost when hard attacking  */
        var hardAttackExtraStaminaCost = 30f

        var dashingStaminaCost = 35f
        var defendingConstantStaminaCost = 12f

        var defenseBrokenStaggerTime = 2.1f
        var gotHitStaggerTime = 1f
        /** [.gotHitStaggerTime] will be reduced by this quantity each time the fighter gets hit again
         * in the period of time specified by [.gotHitStreakLimit]  */
        var consecutiveGotHitStaggerReduction = 0.3f

        var defendingWalkingSpeed = 0.37f

        var staminaGainIdle = 41f
        var staminaGainWalking = 41f
        var staminaGainStagger = 41f
        var staminaGainWhenDashingAvoidingAttack = 22f

        // all hits size as seen from the hit box created looking to the right
        var lightAttackHitWidth = Engine.pixelsToUnits(19f)
        var lightAttackHitHeight = Engine.pixelsToUnits(16f)
        var hardAttackHitWidth = Engine.pixelsToUnits(22f)
        var hardAttackHitHeight = Engine.pixelsToUnits(20f)

        /** estimated reach added to hard attacks due to the initial push  */
        var hardAttackExtraWidth = 1.6f

        var walkingSpeed = 2.25f
    }

    var isAttackInputBufferOn = false
        private set
    private var attackInputBufferTimer = 0f

    /** buffer for when the attack input is pressed while on attack state, time before ending it  */
    fun activateAttackInputBuffer() {
        this.isAttackInputBufferOn = true
        this.attackInputBufferTimer = 0.6f // timer used in case the buffer is left activated
    }

    /** call when finished attack movement without interruptions, normally on its state.
     * activates a timer for detecting combos (input attacks close in time)  */
    fun finishedAttackMovement() {
        attackForComboDelay = attackMaxDelayForCombo
    }

    //region Hit Code
    /** called by the hitting box when it hits the Fighter  */
    fun hit(hittingBox: HittingBox) {
        // already was hit by this one
        if (hitBy.contains(hittingBox, true)) return

        hitBy.add(hittingBox)

        if (hittingBox.side === fighter.side) return
        if (invincible) return

        // invincible by dash
        if (fighter.stateMachine.currentState === fighter.fighterStateDash && fighter.fighterStateDash.isInvincible) {
            println("dodged hit by dashing = stamina bonus")
            currentStamina += config.staminaGainWhenDashingAvoidingAttack

            return
        }

        val hitDirection = Utils.getAngleBetweenPoints(hittingBox.attacker.collidingEntity.position,
                fighter.collidingEntity.position)

        // is defending?
        if (fighter.stateMachine.currentState === fighter.fighterStateDefending) {
            if (doesHittingBoxHitThisShield(hittingBox)) {
                fighter.topDownEntityModule.pushEntity(hittingBox.damage / 9f, 45f, hitDirection)

                currentStamina -= getStaminaLossFromDefendingAttack(hittingBox.damage * 1.4f, config.shieldStrength)
                if (currentStamina < 0) { // defense broken
                    currentStamina = 0f
                    fighter.fighterStateStagger.init(config.defenseBrokenStaggerTime)
                    fighter.stateMachine.changeState(fighter.fighterStateStagger)
                    return
                }
                return
            }
        }

        fighter.topDownEntityModule.pushEntity(0.95f, 3.3f, hitDirection)
        currentHealth -= hittingBox.damage

        if (gotHitStreakTimer > 0) {
            // after getting hit consecutively, lower the stagger time
            gotHitStaggerActualTime -= config.consecutiveGotHitStaggerReduction
            gotHitStaggerActualTime = Math.max(0f, gotHitStaggerActualTime)
            println(gotHitStaggerActualTime)

            gameDrawer.whenNotNull {
                it.drawFlashingTextInUI("Hit Strike!", fighter.collidingEntity.x,
                        fighter.collidingEntity.y + 1.2f, FlashingThing(Color.YELLOW, 0.4f, 1f))
            }
        } else {
            if (fighter.stateMachine.currentState === fighter.fighterStateStagger) {
                // was already staggered = first stagger was not because of hit; don't change the time
                gotHitStaggerActualTime = fighter.fighterStateStagger.staggerTimer.elapsed
            } else {
                // got staggered cause of hit
                gotHitStaggerActualTime = config.gotHitStaggerTime
            }
        }

        fighter.fighterStateStagger.init(gotHitStaggerActualTime)
        fighter.stateMachine.changeState(fighter.fighterStateStagger)

        gotHitStreakTimer = gotHitStreakLimit

        gameDrawer?.drawFlashingTextInUI("hit! " + hittingBox.damage + "dmg", fighter.collidingEntity.x, fighter.collidingEntity.y + 1f,
                FlashingThing(Color.RED, 0.3f, 0.94f))
    }

    private fun doesHittingBoxHitThisShield(hittingBox: HittingBox): Boolean {
        val foeDirection = hittingBox.direction
        val thisBB = fighter.fighterEntityModule.hitBox
        val thisBBbottom = thisBB.y
        val thisBBleft = thisBB.x
        val thisBBright = thisBB.x + thisBB.width
        val thisBBtop = thisBB.y + thisBB.height
        var foeFacingY = 0f
        var foeFacingX = 0f
        val foeBB = hittingBox.attacker.fighterEntityModule.hitBox

        if (foeDirection === TopDownEntityModule.Direction.UP) {
            foeFacingY = foeBB.y + foeBB.height
            foeFacingX = foeBB.x + foeBB.width / 2f
        } else if (foeDirection === TopDownEntityModule.Direction.DOWN) {
            foeFacingY = foeBB.y
            foeFacingX = foeBB.x + foeBB.width / 2f
        } else if (foeDirection === TopDownEntityModule.Direction.LEFT) {
            foeFacingY = foeBB.y + foeBB.height / 2f
            foeFacingX = foeBB.x
        } else if (foeDirection === TopDownEntityModule.Direction.RIGHT) {
            foeFacingY = foeBB.y + foeBB.height / 2f
            foeFacingX = foeBB.x + foeBB.width
        }

        gameDrawer?.drawFlashingRectangleInUI(Rectangle(foeFacingX, foeFacingY, 0.04f, 0.04f),
                    FlashingThing(Color.WHITE, 0.6f, 1f))

        when (facingDirection) {
            TopDownEntityModule.Direction.UP -> return foeDirection !== TopDownEntityModule.Direction.UP && foeFacingY > thisBBtop && !(foeFacingX < thisBB.x + thisBB.width && foeFacingX > thisBB.x && foeDirection !== TopDownEntityModule.Direction.DOWN)
            TopDownEntityModule.Direction.DOWN -> return foeDirection !== TopDownEntityModule.Direction.DOWN && foeFacingY < thisBBbottom && !(foeFacingX < thisBB.x + thisBB.width && foeFacingX > thisBB.x && foeDirection !== TopDownEntityModule.Direction.UP)
            TopDownEntityModule.Direction.LEFT -> return foeDirection !== TopDownEntityModule.Direction.LEFT && foeFacingX < thisBBleft && !(foeFacingY > thisBB.y && foeFacingY < thisBB.y + thisBB.height && foeDirection !== TopDownEntityModule.Direction.RIGHT)
            TopDownEntityModule.Direction.RIGHT -> return foeDirection !== TopDownEntityModule.Direction.RIGHT && foeFacingX > thisBBright && !(foeFacingY > thisBB.y && foeFacingY < thisBB.y + thisBB.height && foeDirection !== TopDownEntityModule.Direction.LEFT)
        }

        return true
    }

    private val invincibleTimer = Timer(0f)

    fun setInvincible(time: Float) {
        invincibleTimer.resetTimer()
        invincibleTimer.timeLimit = time
        invincible = true
    }

    fun update(delta: Float) {
        if (gotHitStreakTimer > 0) {
            gotHitStreakTimer -= delta
        }

        val state = fighter.stateMachine.currentState

        if (lockStaminaRegeneration <= 0) {
            if (state === fighter.fighterStateIdle) {
                currentStamina += delta * config.staminaGainIdle
            } else if (state === fighter.fighterStateWalking) {
                currentStamina += delta * config.staminaGainWalking
            } else if (state === fighter.fighterStateStagger) {
                currentStamina += delta * config.staminaGainStagger
            }
        } else {
            lockStaminaRegeneration -= delta
        }

        if (state === fighter.fighterStateDefending) {
            currentStamina -= delta * config.defendingConstantStaminaCost
        }

        currentStamina = Utils.clamp(currentStamina, -config.maximumStamina / 2, config.maximumStamina)

        changedFacingDirection = false

        // remove killed saved hitting boxes
        val iterator = hitBy.iterator()
        while (iterator.hasNext()) {
            if (iterator.next().isKilled) {
                iterator.remove()
                //System.out.println("remove hittingBox");
            }
        }

        if (invincible) {
            if (invincibleTimer.tick(delta)) {
                invincible = false
            }
        }

        oldFacingDirection = facingDirection

        fighter.topDownEntityModule.update(delta)

        facingDirection = fighter.topDownEntityModule.movingDirection ?: facingDirection

        if (lockedDirection != null) {
            facingDirection = getLockedDirection()
        }

        if (oldFacingDirection !== facingDirection) {
            changedFacingDirection = true
            //            System.out.println("changed from " + oldFacingDirection + " to " + facingDirection);
        }

        if (attackForComboDelay > 0) {
            attackForComboDelay -= delta
        }

        if (attackInputBufferTimer > 0) { // use timer to deactivate the buffer if time passed, just in case
            attackInputBufferTimer -= delta
            if (attackInputBufferTimer <= 0) {
                isAttackInputBufferOn = false
            }
        }

        if (!blocked) {
            if (inputModule.inputAttack || isAttackInputBufferOn || inputModule.inputForceHardAttack) {
                val isFirstAttack = attackForComboDelay <= 0
                fighter.fighterStateAttacking.init(isFirstAttack, inputModule.inputForceHardAttack)
                fighter.stateMachine.changeState(fighter.fighterStateAttacking)
                isAttackInputBufferOn = false

                if (!isFirstAttack && gameDrawer != null && Engine.DEBUG)
                    gameDrawer.drawFlashingTextInUI("combo!", fighter.collidingEntity.x + 1f,
                            fighter.collidingEntity.y + 0.5f, FlashingThing(Color.BLACK, 0.15f, 0.93f))
            } else {
                if (fighter.stateMachine.currentState !== fighter.fighterStateDefending) {
                    if (fighter.topDownEntityModule.inputModule.anyMovementIsTrue()) {
                        if (fighter.stateMachine.currentState === fighter.fighterStateIdle) {
                            fighter.stateMachine.changeState(fighter.fighterStateWalking)
                        }
                    } else {
                        if (fighter.stateMachine.currentState === fighter.fighterStateWalking) {
                            fighter.stateMachine.changeState(fighter.fighterStateIdle)
                        }
                    }

                    if (inputModule.inputDefend) {
                        fighter.stateMachine.changeState(fighter.fighterStateDefending)
                    }
                }

                if (inputModule.inputDash) {
                    fighter.stateMachine.changeState(fighter.fighterStateDash)
                }

                if (inputModule.inputLockDirection) {
                    findEnemyToLockDirectionOnto()
                }
            }
        }
    }

    fun lockStaminaRegeneration(time: Float) {
        this.lockStaminaRegeneration = time
    }

    //region LockingDirection stuff
    /** finds closest foe based on input direction  */
    private fun findEnemyToLockDirectionOnto() {
        val thisBB = fighter.collidingEntity.boundingBox
        val thisBBbottom = thisBB.y
        val thisBBleft = thisBB.x
        val thisBBright = thisBB.x + thisBB.width
        val thisBBtop = thisBB.y + thisBB.height
        val sight = Rectangle()
        val sizePerpendicular = 2.5f // size for each side
        val sizePararel = 5f

        var chosenDirection = fighter.topDownEntityModule.movingDirection ?: facingDirection
        // if not moving
        if (!fighter.topDownEntityModule.inputModule.anyMovementIsTrue()) chosenDirection = facingDirection

        when (chosenDirection) {
            TopDownEntityModule.Direction.UP -> sight.set(thisBBleft + thisBB.width / 2f - sizePerpendicular, thisBBtop, sizePerpendicular * 2, sizePararel)
            TopDownEntityModule.Direction.DOWN -> sight.set(thisBBleft + thisBB.width / 2f - sizePerpendicular, thisBBbottom - sizePararel, sizePerpendicular * 2, sizePararel)
            TopDownEntityModule.Direction.LEFT -> sight.set(thisBBleft - sizePararel, thisBBbottom + thisBB.height / 2f - sizePerpendicular, sizePararel, sizePerpendicular * 2)
            TopDownEntityModule.Direction.RIGHT -> sight.set(thisBBright, thisBBbottom + thisBB.height / 2f - sizePerpendicular, sizePararel, sizePerpendicular * 2)
        }

        gameDrawer?.drawFlashingRectangleInUI(Rectangle(sight),
                FlashingThing(Color.GOLD, 0.23f, 0.35f))

        val foes = Array<Fighter>()
        for (possibleFoe in fighter.collidingEntity.map.collisionTree
                .getPossibleCollidingEntities(sight, true)) {
            if (possibleFoe is Fighter) {
                if ((possibleFoe as Fighter).side !== this.fighter.side) {
                    foes.add(possibleFoe as Fighter)
                }
            }
        }

        var closestFoe: Fighter? = null
        var closest = java.lang.Float.MAX_VALUE
        for (fighter in foes) {
            if (fighter === lockedDirection) continue
            val angleBetween = Utils.getAngleBetweenPoints(this.fighter.collidingEntity.position,
                    fighter.collidingEntity.position)
            if (Utils.getAngleDifferenceNotSigned(chosenDirection.toDegrees().toFloat(), angleBetween) > 77) continue

            val distance = Utils.getDistanceBetweenPoints(this.fighter.collidingEntity.position,
                    fighter.collidingEntity.position)
            if (distance < closest) {
                closest = distance
                closestFoe = fighter
            }
        }

        lockDirectionToEnemy(closestFoe)
    }

    fun lockDirectionToEnemy(enemy: Fighter?) {
        val previousLocked = lockedDirection
        lockedDirection = enemy

        if (previousLocked == null && lockedDirection != null) {
            changedFacingDirection = true
            facingDirection = getLockedDirection()
        }

        // stopped being locked
        if (lockedDirection == null) {
            fighter.topDownEntityModule.movingDirection = facingDirection
            fighter.topDownEntityModule.changedMovingDirection = true
        }
        //System.out.println(lockedDirection);
    }

    fun unlockDirection() {
        this.lockDirectionToEnemy(null)
    }

    fun hasLockedDirectionOntoEnemy(): Boolean {
        return lockedDirection != null
    }

    private fun getLockedDirection(): TopDownEntityModule.Direction {
        val thisCenter = Pools.obtain(Vector2::class.java)
        fighter.collidingEntity.boundingBox.getCenter(thisCenter)

        val foeCenter = Pools.obtain(Vector2::class.java)
        lockedDirection!!.collidingEntity.boundingBox.getCenter(foeCenter)

        val direction = Utils.getAngleBetweenPoints(thisCenter, foeCenter)

        Pools.free(thisCenter)
        Pools.free(foeCenter)

        return TopDownEntityModule.Direction.getClosestDirectionFromDegrees(direction)
    }
    //endregion

    inner class InputModule {
        var inputAttack: Boolean = false

        /** ignore for A.I., this should be set to true each frame when the button for attack is down; unlike
         * [.inputAttack] which should be set to true only when just pressed
         * This is used to detect hard attack input; when this is true all frames until a certain point in the attack animation  */
        var inputAttackHold: Boolean = false
        /** This forces a hard attack when attacking. If not forced, you can hard attack with [.inputAttackHold]  */
        var inputForceHardAttack: Boolean = false
        var inputDash: Boolean = false
        var inputDefend: Boolean = false
        var inputLockDirection: Boolean = false

        fun resetInputBooleans() {
            inputAttackHold = false
            inputForceHardAttack = inputAttackHold
            inputLockDirection = inputForceHardAttack
            inputDefend = inputLockDirection
            inputDash = inputDefend
            inputAttack = inputDash
        }
    }

    fun drawDebugFighterInfo(gameDrawer: GameDrawer, delta: Float) {
        if (fighter.stateMachine.currentState === fighter.fighterStateDash) {
            gameDrawer.alpha = 0.5f
            gameDrawer.color = Color.LIGHT_GRAY
            gameDrawer.resetAlpha()
        } else if (fighter.stateMachine.currentState === fighter.fighterStateAttacking) {
            gameDrawer.color = Color.BLUE
        }
        if (invincible) {
            gameDrawer.color = Color.GOLD
        }
        if (fighter.stateMachine.currentState === fighter.fighterStateAttacking) {
            gameDrawer.color = Color.BLUE
        }

        //        gameDrawer.drawTextInUI(gameDrawer.getWorldViewport(),
        //                fighter.getCollidingEntity().getX() + 0.4f,
        //                fighter.getCollidingEntity().getY() - 0.1f,
        //                fighter.getCollidingEntity().getPosition().toString());

        Utils.drawProgressBar(gameDrawer, fighter.collidingEntity.x - 0.1f,
                fighter.collidingEntity.y + 1.3f, 0.7f, 0.1f,
                currentHealth, config.maximumHealth, 0f, Color.BLACK, Color.RED)
        gameDrawer.drawTextInUI(fighter.collidingEntity.x + 0.45f,
                fighter.collidingEntity.y + 1.15f, Utils.formatNumber(currentHealth)
                + "/ " + Utils.formatNumber(config.maximumHealth))

        Utils.drawProgressBar(gameDrawer, fighter.collidingEntity.x - 0.1f,
                fighter.collidingEntity.y + 1.2f, 0.7f, 0.1f,
                currentStamina, config.maximumStamina, 0f, Color.BLACK, Color.GREEN)
        gameDrawer.drawTextInUI(fighter.collidingEntity.x + 0.45f,
                fighter.collidingEntity.y + 0.97f, Utils.formatNumber(currentStamina)
                + "/ " + Utils.formatNumber(config.maximumStamina))
    }

    companion object {

        val hitBoxBBname = "hitBox"

        fun getStaminaLossFromDefendingAttack(attackDamage: Float, shieldStrength: Float): Float {
            return attackDamage / shieldStrength * 5
        }

        /** Creates the Rectangle for hits as if the fighter was in the direction specified  */
        @JvmOverloads
        fun getHittingBoxRectangle(fighter: Fighter, fighterDirection: TopDownEntityModule.Direction,
                                   hardAttackBB: Boolean, hittingBox: Rectangle, relative: Boolean = false): Rectangle {
            val bbWidth: Float
            val bbHeight: Float
            if (hardAttackBB) {
                bbWidth = fighter.fighterEntityModule.config.hardAttackHitWidth
                bbHeight = fighter.fighterEntityModule.config.hardAttackHitHeight
            } else {
                bbWidth = fighter.fighterEntityModule.config.lightAttackHitWidth
                bbHeight = fighter.fighterEntityModule.config.lightAttackHitHeight
            }

            when (fighterDirection) {
                TopDownEntityModule.Direction.DOWN -> hittingBox.set(-bbHeight / 2f, -bbWidth, bbHeight, bbWidth)
                TopDownEntityModule.Direction.UP -> hittingBox.set(-bbHeight / 2f, 0f, bbHeight, bbWidth)
                TopDownEntityModule.Direction.LEFT -> hittingBox.set(-bbWidth, -bbHeight / 2f, bbWidth, bbHeight)
                TopDownEntityModule.Direction.RIGHT -> hittingBox.set(0f, -bbHeight / 2f, bbWidth, bbHeight)
            }

            if (!relative) {
                // move from relative to absolute coords
                val origin = Pools.obtain(Vector2::class.java)
                getOuterCenter(fighter, fighterDirection, origin)
                hittingBox.x += origin.x
                hittingBox.y += origin.y
                Pools.free(origin)
            }

            return hittingBox
        }
        //endregion

        fun getOuterCenter(fighter: Fighter, direction: TopDownEntityModule.Direction, vector2: Vector2): Vector2 {
            val bb = fighter.fighterEntityModule.hitBox
            when (direction) {
                TopDownEntityModule.Direction.DOWN -> vector2.set(bb.x + bb.width / 2f, bb.y)
                TopDownEntityModule.Direction.UP -> vector2.set(bb.x + bb.width / 2f, bb.y + bb.height)
                TopDownEntityModule.Direction.LEFT -> vector2.set(bb.x, bb.y + bb.height / 2f)
                TopDownEntityModule.Direction.RIGHT -> vector2.set(bb.x + bb.width, bb.y + bb.height / 2f)
            }

            return vector2
        }
    }
}
/** Creates the (absolutely positioned) Rectangle for hits as if the fighter was in the direction specified  */
