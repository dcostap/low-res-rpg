package com.dcostap.game.game_screen.entities.fighter

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.dcostap.engine_2d.engine.utils.FlashingThing
import com.dcostap.engine_2d.engine.utils.Timer
import com.dcostap.engine_2d.engine.utils.finite_state_machine.State
import com.dcostap.game.game_screen.entities.HittingBox

/**
 * Created by Darius on 19/02/2018.
 */
class FighterStateAttacking : State<Fighter> {
    private var playerHit: HittingBox? = null

    private val animationTimer = Timer(50f, true, true)
    private var startMoving: Boolean = false
    private var moved: Boolean = false
    private var createdHit: Boolean = false
    private var isFirstAttack: Boolean = false
    var isHardAttack: Boolean = false
        private set
    private var attackInputCounterForHardAttack: Boolean = false
    private var consumedHardAttackStamina: Boolean = false

    var hitSurvivalTime = 0.11f
    private val minimumTimeUntilAttackBuffer = 0.18f

    var lightAttackMovingSpeed = 0f
    var lightAttackMovingFriction = 0f
    var hardAttackMovingSpeed = 0f
    var hardAttackMovingFriction = 0f
    var state1startMovingTime = 0f
    var state2createHitTime = 0f
    var state3finishTime = 0f
    var comboState1startMovingTime = 0f
    var comboState2createHitTime = 0f
    var comboState3finishTime = 0f
    var hardAttackState1startMovingTime = 0f
    var hardAttackState2createHitTime = 0f
    var hardAttackState3finishTime = 0f

    var combosAllowed: Boolean = false

    /** override to change config  */
    fun configure() {
        lightAttackMovingSpeed = 1f
        lightAttackMovingFriction = 5f

        hardAttackMovingSpeed = 16f
        hardAttackMovingFriction = 57f

        state1startMovingTime = 0.33f
        state2createHitTime = 0.42f
        state3finishTime = 0.72f

        comboState1startMovingTime = 0.15f
        comboState2createHitTime = 0.2f
        comboState3finishTime = 0.52f

        hardAttackState1startMovingTime = 0.65f
        hardAttackState2createHitTime = 0.8f
        hardAttackState3finishTime = 1.2f

        combosAllowed = true
    }

    fun init(isFirstAttack: Boolean, forceHardAttack: Boolean) {
        this.isFirstAttack = isFirstAttack
        this.isHardAttack = forceHardAttack
    }

    override fun enter(being: Fighter) {
        if (being.fighterEntityModule.currentStamina <= 0) {
            being.fighterEntityModule.lockStaminaRegeneration(0.15f)
            being.stateMachine.changeState(being.fighterStateIdle)
            return
        }

        var staminaCost = being.fighterEntityModule.config.lightAttackStaminaCost
        staminaCost += if (isHardAttack) being.fighterEntityModule.config.hardAttackExtraStaminaCost else 0f
        being.fighterEntityModule.currentStamina -= staminaCost

        configure()

        startMoving = false
        moved = false
        being.fighterEntityModule.blocked = true
        being.topDownEntityModule.walkingSpeed = 0f
        createdHit = false
        animationTimer.resetTimer()

        this.attackInputCounterForHardAttack = true
        consumedHardAttackStamina = false

        being.fighterAnimation.attackStart(this)
    }

    override fun execute(being: Fighter, delta: Float) {
        animationTimer.tick(delta)

        being.fighterAnimation.attackExecute(this)

        // counter for pressing attack since start of state; allows to hard attack while keeping pressed the input
        if (!being.fighterEntityModule.inputModule.inputAttackHold) {
            attackInputCounterForHardAttack = false // stopped pressing
        }

        if (startMoving && !moved) {
            being.topDownEntityModule.pushEntity(
                    if (isHardAttack) hardAttackMovingSpeed else lightAttackMovingSpeed,
                    if (isHardAttack) hardAttackMovingFriction else lightAttackMovingFriction,
                    being.fighterEntityModule.facingDirection!!.toDegrees().toFloat())
            moved = true
        }

        if (isHardAttack) {
            if (!consumedHardAttackStamina) {
                being.fighterEntityModule.currentStamina -= being.fighterEntityModule.config.hardAttackExtraStaminaCost
                consumedHardAttackStamina = true
            }
            updateHardAttack(being, delta)
        } else {
            updateLightAttack(being, delta)
        }
    }

    protected fun updateLightAttack(being: Fighter, delta: Float) {
        val isCombo = isComboLightAttack

        if (animationTimer.elapsed > (if (isCombo) comboState3finishTime else state3finishTime) && createdHit) {
            being.stateMachine.changeState(being.fighterStateIdle)
            being.fighterEntityModule.finishedAttackMovement()
        }
        if (animationTimer.elapsed > (if (isCombo) comboState2createHitTime else state2createHitTime) + hitSurvivalTime && createdHit) {
            resetPlayerHit()
        }
        if (animationTimer.elapsed > (if (isCombo) comboState2createHitTime else state2createHitTime)) {
            if (!createdHit) {
                createHittingBox(being)
                createdHit = true
            }
        }
        if (animationTimer.elapsed > (if (isCombo) comboState1startMovingTime else state1startMovingTime)) {
            // pressed attack button since start of animation = switch to a hard attack
            if (attackInputCounterForHardAttack) {
                isHardAttack = true

                if (being.fighterEntityModule.gameDrawer != null) {
                    being.fighterEntityModule.gameDrawer!!.drawFlashingTextInUI("Hard Attack!", being.collidingEntity.x, being.collidingEntity.y + 1f,
                            FlashingThing(Color.BLACK, 0.5f, 0.94f))
                }
                return
            }
            startMoving = true
        }

        if (animationTimer.elapsed > Math.max(if (isCombo) comboState2createHitTime else state2createHitTime, minimumTimeUntilAttackBuffer)) {
            if (being.fighterEntityModule.inputModule.inputAttack) {
                being.fighterEntityModule.activateAttackInputBuffer()
            }
        }
    }

    protected fun updateHardAttack(being: Fighter, delta: Float) {
        if (animationTimer.elapsed > hardAttackState3finishTime && createdHit) {
            being.stateMachine.changeState(being.fighterStateIdle)
            being.fighterEntityModule.finishedAttackMovement()
        }
        if (animationTimer.elapsed > hardAttackState2createHitTime + hitSurvivalTime && createdHit) {
            resetPlayerHit()
        }
        if (animationTimer.elapsed > hardAttackState2createHitTime) {
            if (!createdHit) {
                createHittingBox(being)
                createdHit = true
            }
        }
        if (animationTimer.elapsed > hardAttackState1startMovingTime) {
            startMoving = true
        }

        if (animationTimer.elapsed > Math.max(hardAttackState3finishTime / 2f, minimumTimeUntilAttackBuffer)) {
            if (being.fighterEntityModule.inputModule.inputAttack)
                being.fighterEntityModule.activateAttackInputBuffer()
        }
    }

    protected fun createHittingBox(being: Fighter) {
        val hittingBox = Rectangle()
        val origin = Vector2()
        FighterEntityModule.getOuterCenter(being, being.fighterEntityModule.facingDirection, origin)

        FighterEntityModule.getHittingBoxRectangle(being, being.fighterEntityModule.facingDirection, isHardAttack, hittingBox, true)

        fun attackStrength(): Float {
            return if (isHardAttack)
                being.fighterEntityModule.config.hardAttackStrength
            else
                being.fighterEntityModule.config.lightAttackStrength
        }

        playerHit = HittingBox(origin, hittingBox,
                being.collidingEntity.map, attackStrength(),
                being, being.fighterEntityModule.facingDirection)

        being.collidingEntity.map.addEntity(playerHit!!)
    }

    val isComboLightAttack: Boolean
        get() = !isFirstAttack && combosAllowed

    private fun resetPlayerHit() {
        if (playerHit != null) {
            playerHit!!.kill()
            playerHit = null
        }
    }

    override fun exit(being: Fighter) {
        resetPlayerHit()
        being.fighterEntityModule.blocked = false
        isFirstAttack = true
        isHardAttack = false
    }

    val animationTimerElapsed: Float
        get() = animationTimer.elapsed
}
