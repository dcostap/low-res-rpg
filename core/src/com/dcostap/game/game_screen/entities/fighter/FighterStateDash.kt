package com.dcostap.game.game_screen.entities.fighter

import com.dcostap.engine_2d.engine.utils.Timer
import com.dcostap.engine_2d.engine.utils.Utils
import com.dcostap.engine_2d.engine.utils.finite_state_machine.State
import com.dcostap.game.game_screen.entities.TopDownEntityModule

/**
 * Created by Darius on 19/02/2018.
 */
class FighterStateDash: State<Fighter> {
    private var speed: Float = 0.toFloat()
    private var normalSpeed: Float = 0.toFloat()
    private val timer = Timer(0.24f, true, false)
    private val totalTimer = Timer(100f, true, false)
    private var finished: Boolean = false
    var isInvincible: Boolean = false
        private set

    fun updateWalkingAnimation() {

    }

    override fun enter(being: Fighter) {
        if (being.fighterEntityModule.currentStamina <= 0) {
            being.stateMachine.changeState(being.fighterStateIdle)
            return
        }

        // ugly hack to make topDown module update walking direction, from the input
        // this allows to dash in correct direction when you just dashed after getting out of another state
        // (without this the topDown module won't update direction and dash will occur on facing direction)
        being.topDownEntityModule.walkingSpeed = 1f
        being.topDownEntityModule.update(1f)
        being.collidingEntity.setSpeed(0f, 0f)

        being.fighterEntityModule.currentStamina -= being.fighterEntityModule.config.dashingStaminaCost

        updateWalkingAnimation()

        being.fighterEntityModule.blocked = true

        speed = 0.07f
        normalSpeed = 0.032f
        timer.resetTimer()
        finished = false

        being.fighterEntityModule.setInvincible(0.36f)
        isInvincible = false

        being.topDownEntityModule.walkingSpeed = 0f
    }

    override fun execute(being: Fighter, delta: Float) {
        totalTimer.tick(delta)
        if (totalTimer.elapsed > 0.1) {
            isInvincible = false
        } else if (totalTimer.elapsed > 0.01f) {
            isInvincible = true
        }

        if (finished) {
            speed -= Utils.mapToRange(speed, 0.01f, 0.5f, 0.05f, 0.45f) * delta
            if (speed < 0.002f) {
                being.stateMachine.changeState(being.fighterStateIdle)
            }
        } else {
            speed = Math.max(normalSpeed, speed - 5 * delta)
            if (timer.tick(delta)) {
                finished = true
            }
        }

        when (being.topDownEntityModule.movingDirection) {
            TopDownEntityModule.Direction.DOWN -> being.collidingEntity.moveColliding(0f, -speed, 1f)
            TopDownEntityModule.Direction.UP -> being.collidingEntity.moveColliding(0f, speed, 1f)
            TopDownEntityModule.Direction.LEFT -> being.collidingEntity.moveColliding(-speed, 0f, 1f)
            TopDownEntityModule.Direction.RIGHT -> being.collidingEntity.moveColliding(speed, 0f, 1f)
        }
    }

    override fun exit(being: Fighter) {
        being.fighterEntityModule.blocked = false
    }

}
