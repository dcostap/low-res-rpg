package com.dcostap.game.game_screen.entities.fighter

import com.dcostap.engine_2d.engine.utils.Timer
import com.dcostap.engine_2d.engine.utils.finite_state_machine.State

/**
 * Created by Darius on 19/02/2018.
 */
class FighterStateDefending: State<Fighter> {

    internal var defendingMinimumTime = Timer(0.25f, true, true)

    override fun enter(being: Fighter) {
        //        being.getFighterEntityModule().blocked = true;
        being.topDownEntityModule.walkingSpeed = being.fighterEntityModule.config.defendingWalkingSpeed

        defendingMinimumTime.resetTimer()
        defendingMinimumTime.turnTimerOn()

        being.fighterAnimation.defendingStart()
    }

    override fun execute(being: Fighter, delta: Float) {
        defendingMinimumTime.tick(delta)

        being.fighterAnimation.defendingExecute()

        if (!being.fighterEntityModule.inputModule.inputDefend && !defendingMinimumTime.isTimerOn) {
            being.stateMachine.changeState(being.fighterStateIdle)
        }
    }

    override fun exit(being: Fighter) {
        //        being.getFighterEntityModule().blocked = false;
    }

}
