package com.dcostap.game.game_screen.entities.fighter

import com.dcostap.engine_2d.engine.utils.finite_state_machine.State

/**
 * Created by Darius on 19/02/2018.
 */
class FighterStateIdle : State<Fighter> {
    override fun enter(being: Fighter) {
        being.fighterAnimation.idleStart()
    }

    override fun execute(being: Fighter, delta: Float) {
        being.fighterAnimation.idleExecute()
    }

    override fun exit(being: Fighter) {

    }

}
