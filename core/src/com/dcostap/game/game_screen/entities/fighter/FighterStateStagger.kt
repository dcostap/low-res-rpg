package com.dcostap.game.game_screen.entities.fighter

import com.badlogic.gdx.graphics.Color
import com.dcostap.engine_2d.engine.Engine
import com.dcostap.engine_2d.engine.utils.FlashingThing
import com.dcostap.engine_2d.engine.utils.Timer
import com.dcostap.engine_2d.engine.utils.finite_state_machine.State

/**
 * Created by Darius on 06/03/2018.
 */
class FighterStateStagger : State<Fighter> {
    fun init(timerLimit: Float) {
        staggerTimer.timeLimit = timerLimit
    }

    var staggerTimer = Timer(0f)

    override fun enter(being: Fighter) {
        being.fighterEntityModule.blocked = true
        being.topDownEntityModule.walkingSpeed = 0f

        staggerTimer.resetTimer()

        if (Engine.DEBUG && being.fighterEntityModule.gameDrawer != null) {
            being.fighterEntityModule.gameDrawer!!.drawFlashingTextInUI("stagger!", being.collidingEntity.x, being.collidingEntity.y + 0.65f,
                    FlashingThing(Color.RED, staggerTimer.timeLimit, 0.54f))
        }
    }

    override fun execute(being: Fighter, delta: Float) {
        if (staggerTimer.tick(delta)) {
            being.stateMachine.changeState(being.fighterStateIdle)
        }
    }

    override fun exit(being: Fighter) {
        being.fighterEntityModule.blocked = false
    }

}
