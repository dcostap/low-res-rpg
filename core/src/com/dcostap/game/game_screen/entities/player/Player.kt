package com.dcostap.game.game_screen.entities.player

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.dcostap.engine_2d.engine.map.EntityTiledMap
import com.dcostap.engine_2d.engine.utils.GameDrawer
import com.dcostap.engine_2d.engine.utils.whenNotNull
import com.dcostap.game.game_screen.GameScreen
import com.dcostap.game.game_screen.entities.HittingBox
import com.dcostap.game.game_screen.entities.fighter.Fighter
import com.dcostap.game.game_screen.entities.fighter.FighterBase

/**
 * Created by Darius on 11/02/2018.
 */
class Player(position: Vector2, map: EntityTiledMap, var gameScreen: GameScreen)
    : FighterBase(position, Rectangle(), map, false, gameScreen.gameDrawer), Fighter
{
    private var clickPressed = false
    override val side
        get() = HittingBox.Side.ALLIED
    override val fighterAnimation = PlayerAnimation(this, gameScreen.game.gameAssets)

    init {
        setup()
    }

    override fun update(delta: Float) {
        super.update(delta)

        gameScreen.camera.position.set(this.x, this.y, 0f)

        checkInput()

        fighterEntityModule.update(delta)
        stateMachine.update(delta)
    }

    private fun checkInput() {
        topDownEntityModule.inputModule.resetInputBooleans()
        fighterEntityModule.inputModule.resetInputBooleans()

        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            topDownEntityModule.inputModule.inputLeft = true
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            topDownEntityModule.inputModule.inputRight = true
        }
        if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            topDownEntityModule.inputModule.inputUp = true
        }
        if (Gdx.input.isKeyPressed(Input.Keys.S)) {
            topDownEntityModule.inputModule.inputDown = true
        }

        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            fighterEntityModule.inputModule.inputDash = true
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.SHIFT_LEFT)) {
            fighterEntityModule.inputModule.inputLockDirection = true
        }
        if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
            fighterEntityModule.inputModule.inputAttackHold = true

            // only when just pressed
            if (!clickPressed) {
                fighterEntityModule.inputModule.inputAttack = true
            }
            clickPressed = true
        } else {
            clickPressed = false
        }
        if (Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) {
            fighterEntityModule.inputModule.inputDefend = true
        }
    }

    //region draw stuff
    override fun draw(gameDrawer: GameDrawer, delta: Float) {
        fighterAnimation.draw(gameDrawer, delta)

        fighterEntityModule.lockedDirection.whenNotNull {
            val rectangle = Rectangle(it.collidingEntity.getBoundingBox())
            rectangle.x -= 0.2f
            rectangle.y -= 0.2f
            rectangle.width += 0.2f * 2
            rectangle.height += 0.2f * 2
            gameDrawer.alpha = 0.4f
            gameDrawer.drawRectangle(rectangle, 0.05f, false)
            gameDrawer.resetAlpha()
        }

        fighterEntityModule.drawDebugFighterInfo(gameDrawer, delta)

        super.draw(gameDrawer, delta)
    }

    //endregion
}
