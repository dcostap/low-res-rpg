package com.dcostap.game.game_screen.entities.player

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.dcostap.engine_2d.engine.Assets
import com.dcostap.engine_2d.engine.utils.GameDrawer
import com.dcostap.engine_2d.engine.utils.TextureAnimation
import com.dcostap.engine_2d.engine.utils.whenNotNull
import com.dcostap.game.game_screen.entities.TopDownEntityModule
import com.dcostap.game.game_screen.entities.fighter.Fighter
import com.dcostap.game.game_screen.entities.fighter.FighterAnimation
import com.dcostap.game.game_screen.entities.fighter.FighterStateAttacking
import com.badlogic.gdx.graphics.g2d.TextureAtlas as txt

/**
 * Created by Darius on 17/03/2018.
 *
 * modifies Entity's bounding box!
 */
class PlayerAnimation(private val fighter: Fighter, private val assets: Assets) : FighterAnimation {
    private val attackDown = TextureAnimation(assets.findRegions("sk_sword_down"))
    private val attackRight = TextureAnimation(assets.findRegions("sk_sword_right"))
    private val attackUp = TextureAnimation(assets.findRegions("sk_sword_up"))
    private val walkingDown = TextureAnimation(assets.findRegions("sk_walking_down"))
    private val walkingRight = TextureAnimation(assets.findRegions("sk_walking_right"))
    private val walkingUp = TextureAnimation(assets.findRegions("sk_walking_up"))
    private val defendingDown = TextureAnimation(assets.findRegions("sk_shield_down"))
    private val defendingRight = TextureAnimation(assets.findRegions("sk_shield_right"))
    private val defendingUp = TextureAnimation(assets.findRegions("sk_shield_up"))

    private val downMotionBlur1 = assets.findRegion("sk_down_sword_motionBlur1")
    private val downMotionBlur2 = assets.findRegion("sk_down_sword_motionBlur2")
    private val rightMotionBlur1 = assets.findRegion("sk_sword_right_motionBlur1")
    private val rightMotionBlur2 = assets.findRegion("sk_sword_right_motionBlur2")
    private val upMotionBlur1 = assets.findRegion("sk_up_sword_motionBlur1")
    private val upMotionBlur2 = assets.findRegion("sk_up_sword_motionBlur2")
    private val shieldDown = assets.findRegion("shield_down")
    private val shieldUp = assets.findRegion("shield_up")
    private val shieldRight = assets.findRegion("shield_right")

    private var currentAnimation: TextureAnimation<txt.AtlasRegion>

    init {
        fighter.collidingEntity.actualBoundingBox.modifyRelativeBoundingBoxPixels(-3f, 0f, 6f, 7f)

        currentAnimation = walkingDown
    }

    fun updateWalkingAnim() {
        when (fighter.fighterEntityModule.facingDirection) {
            TopDownEntityModule.Direction.DOWN -> currentAnimation = walkingDown
            TopDownEntityModule.Direction.LEFT, TopDownEntityModule.Direction.RIGHT -> currentAnimation = walkingRight
            TopDownEntityModule.Direction.UP -> currentAnimation = walkingUp
        }

        currentAnimation.reset()
        currentAnimation.resume()
        currentAnimation.frameDuration = 0.2f
    }

    override fun idleStart() {

    }

    override fun idleExecute() {
        // reset of animation done this way so when you combo-attack there isn't one frame of idle animation in the middle
        if (!fighter.fighterEntityModule.isAttackInputBufferOn) {
            updateWalkingAnim()
            currentAnimation.reset()
            currentAnimation.pause()
        }
    }

    override fun walkingStart() {
        updateWalkingAnim()
    }

    override fun walkingExecute() {
        if (fighter.fighterEntityModule.changedFacingDirection) {
            updateWalkingAnim()
        }
    }

    //region Attack Anim
    fun updateAttackAnim() {
        when (fighter.fighterEntityModule.facingDirection) {
            TopDownEntityModule.Direction.DOWN -> currentAnimation = attackDown
            TopDownEntityModule.Direction.LEFT, TopDownEntityModule.Direction.RIGHT -> currentAnimation = attackRight
            TopDownEntityModule.Direction.UP -> currentAnimation = attackUp
        }
    }

    private var lightAttackComboFirst = false
    private var currentMotionBlur: TextureRegion? = null
    private var flipMotionBlurOnY = false
    private var flipMotionBlurOnX = false

    override fun attackStart(stateAttacking: FighterStateAttacking) {
        updateAttackAnim()
        currentAnimation.pause()
        currentAnimation.reset()

        if (stateAttacking.isComboLightAttack) {
            lightAttackComboFirst = !lightAttackComboFirst
        } else {
            lightAttackComboFirst = false
        }
    }

    override fun attackExecute(stateAttacking: FighterStateAttacking) {
        currentMotionBlur = null
        flipMotionBlurOnY = false
        flipMotionBlurOnX = false

        val direction = fighter.fighterEntityModule.facingDirection

        if (direction === TopDownEntityModule.Direction.LEFT) {
            flipMotionBlurOnX = true
        }

        if (stateAttacking.isComboLightAttack) {
            if (lightAttackComboFirst) {
                if (direction === TopDownEntityModule.Direction.LEFT || direction === TopDownEntityModule.Direction.RIGHT) {
                    flipMotionBlurOnY = true
                } else {
                    flipMotionBlurOnX = true
                }

                val attackSubtime = stateAttacking.comboState2createHitTime / 2
                val finishSubtime = (stateAttacking.comboState3finishTime - stateAttacking.comboState2createHitTime) / 3

                if (stateAttacking.animationTimerElapsed > 0)
                    currentAnimation.setFrame(5)
                // create hit
                if (stateAttacking.animationTimerElapsed > attackSubtime * 2)
                    currentAnimation.setFrame(4)
                if (stateAttacking.animationTimerElapsed > attackSubtime * 2 + 0.05f) {
                    currentAnimation.setFrame(2)
                    currentMotionBlur = getMotionBlur(1)
                }
                // finish
                if (stateAttacking.animationTimerElapsed > attackSubtime * 2 + finishSubtime * 1.1f) {
                    currentAnimation.setFrame(0)

                    if (stateAttacking.animationTimerElapsed > attackSubtime * 2 + finishSubtime * 1.8f) {
                        currentMotionBlur = null
                    } else {
                        currentMotionBlur = getMotionBlur(2)
                    }
                }
                if (stateAttacking.animationTimerElapsed > attackSubtime * 2 + finishSubtime * 2f) {
                    currentAnimation.setFrame(0)
                    currentMotionBlur = null
                }
            } else {
                val attackSubtime = stateAttacking.comboState2createHitTime / 2
                val finishSubtime = (stateAttacking.comboState3finishTime - stateAttacking.comboState2createHitTime) / 3

                if (stateAttacking.animationTimerElapsed > 0)
                    currentAnimation.setFrame(1)
                // create hit
                if (stateAttacking.animationTimerElapsed > attackSubtime * 2)
                    currentAnimation.setFrame(0)
                if (stateAttacking.animationTimerElapsed > attackSubtime * 2 + 0.03f) {
                    currentAnimation.setFrame(3)
                    currentMotionBlur = getMotionBlur(1)
                }
                // finish
                if (stateAttacking.animationTimerElapsed > attackSubtime * 2 + finishSubtime) {
                    currentAnimation.setFrame(4)

                    if (stateAttacking.animationTimerElapsed > attackSubtime * 2 + finishSubtime * 1.8f) {
                        currentMotionBlur = null
                    } else {
                        currentMotionBlur = getMotionBlur(2)
                    }
                }
                if (stateAttacking.animationTimerElapsed > attackSubtime * 2 + finishSubtime * 2.5) {
                    if (!fighter.fighterEntityModule.isAttackInputBufferOn)
                        currentAnimation.setFrame(3)
                    currentMotionBlur = null
                }
            }
        } else { // normal light attack
            var attackSubtime = stateAttacking.state2createHitTime / 2
            var finishSubtime = (stateAttacking.state3finishTime - stateAttacking.state2createHitTime) / 3

            if (stateAttacking.isHardAttack) {
                attackSubtime = stateAttacking.hardAttackState2createHitTime / 2
                finishSubtime = (stateAttacking.hardAttackState3finishTime - stateAttacking.hardAttackState2createHitTime) / 3
            }

            if (stateAttacking.animationTimerElapsed > 0)
                currentAnimation.setFrame(0)
            if (stateAttacking.animationTimerElapsed > attackSubtime)
                currentAnimation.setFrame(1)
            // create hit
            if (stateAttacking.animationTimerElapsed > attackSubtime * 2)
                currentAnimation.setFrame(0)
            if (stateAttacking.animationTimerElapsed > attackSubtime * 2 + 0.03f) {
                currentAnimation.setFrame(3)
                currentMotionBlur = getMotionBlur(1)
            }
            // finish
            if (stateAttacking.animationTimerElapsed > attackSubtime * 2 + finishSubtime) {
                currentAnimation.setFrame(4)

                if (stateAttacking.animationTimerElapsed > attackSubtime * 2 + finishSubtime * 1.8f) {
                    currentMotionBlur = null
                } else {
                    currentMotionBlur = getMotionBlur(2)
                }
            }
            if (stateAttacking.animationTimerElapsed > attackSubtime * 2 + finishSubtime * 2.5) {
                if (!fighter.fighterEntityModule.isAttackInputBufferOn)
                    currentAnimation.setFrame(3)
                currentMotionBlur = null
            }
        }
    }

    private fun getMotionBlur(motionBlurIndex: Int): TextureRegion? {
        if (motionBlurIndex == 1) {
            return when (fighter.fighterEntityModule.facingDirection) {
                TopDownEntityModule.Direction.DOWN -> downMotionBlur1
                TopDownEntityModule.Direction.LEFT, TopDownEntityModule.Direction.RIGHT -> rightMotionBlur1
                TopDownEntityModule.Direction.UP -> upMotionBlur1
            }
        } else {
            return when (fighter.fighterEntityModule.facingDirection) {
                TopDownEntityModule.Direction.DOWN -> downMotionBlur2
                TopDownEntityModule.Direction.LEFT, TopDownEntityModule.Direction.RIGHT -> rightMotionBlur2
                TopDownEntityModule.Direction.UP -> upMotionBlur2
            }
        }
    }
    //endregion

    override fun defendingStart() {
        updateDefendingAnim()
    }

    private fun updateDefendingAnim() {
        when (fighter.fighterEntityModule.facingDirection) {
            TopDownEntityModule.Direction.DOWN -> currentAnimation = defendingDown
            TopDownEntityModule.Direction.LEFT, TopDownEntityModule.Direction.RIGHT -> currentAnimation = defendingRight
            TopDownEntityModule.Direction.UP -> currentAnimation = defendingUp
        }

        currentAnimation.frameDuration = 0.4f
        currentAnimation.reset()
        currentAnimation.pause()
    }

    override fun defendingExecute() {
        if (fighter.topDownEntityModule.isWalking) {
            currentAnimation.resume()
        } else {
            currentAnimation.pause()
            currentAnimation.reset()
        }

        if (fighter.fighterEntityModule.changedFacingDirection) {
            updateDefendingAnim()
        }
    }

    override fun draw(gameDrawer: GameDrawer, delta: Float) {
        if (fighter.stateMachine.currentState === fighter.fighterStateDash) {
            gameDrawer.alpha = (0.7f)
            gameDrawer.color = (Color.LIGHT_GRAY)
        }

        if (fighter.fighterEntityModule.invincible) {
            gameDrawer.color = (Color.GOLD)
        }

        if (fighter.stateMachine.currentState === fighter.fighterStateDefending && fighter.fighterEntityModule.facingDirection === TopDownEntityModule.Direction.UP) {
            drawShield(gameDrawer, delta)
        }

        gameDrawer.setDrawingDisplacementPixels(-20f, -13f)
        gameDrawer.drawWithOriginOnCenter(currentAnimation.getFrame(delta), fighter.collidingEntity.x,
                fighter.collidingEntity.y,
                if (fighter.fighterEntityModule.facingDirection === TopDownEntityModule.Direction.LEFT) -1f else 1f, 1f,
                true, false, 0f)

        if (fighter.stateMachine.currentState === fighter.fighterStateDefending
                && fighter.fighterEntityModule.facingDirection !== TopDownEntityModule.Direction.UP) {
            drawShield(gameDrawer, delta)
        }

        currentMotionBlur.whenNotNull {
            gameDrawer.drawWithOriginOnCenter(it, fighter.collidingEntity.x, fighter.collidingEntity.y,
                    if (flipMotionBlurOnX) -1f else 1f, if (flipMotionBlurOnY) -1f else 1f,
                    true, true, 0f)
        }

        gameDrawer.resetDrawingDisplacement()
        gameDrawer.resetColorAndAlpha()
    }

    private fun drawShield(gameDrawer: GameDrawer, delta: Float) {
        val index = currentAnimation.frameIndex

        var shield: TextureRegion? = null
        when (fighter.fighterEntityModule.facingDirection) {
            TopDownEntityModule.Direction.DOWN -> {
                shield = shieldDown
                when (index) {
                    0 -> gameDrawer.setDrawingDisplacementPixels(-5f, -1f)
                    1 -> gameDrawer.setDrawingDisplacementPixels(-5f, -2f)
                }
            }
            TopDownEntityModule.Direction.LEFT -> {
                shield = shieldRight
                when (index) {
                    0 -> gameDrawer.setDrawingDisplacementPixels(-1f, -0f)
                    1 -> gameDrawer.setDrawingDisplacementPixels(-1f, -1f)
                }
            }
            TopDownEntityModule.Direction.RIGHT -> {
                shield = shieldRight
                when (index) {
                    0 -> gameDrawer.setDrawingDisplacementPixels(2f, -0f)
                    1 -> gameDrawer.setDrawingDisplacementPixels(2f, -1f)
                }
            }
            TopDownEntityModule.Direction.UP -> {
                shield = shieldUp
                when (index) {
                    0, 2 -> gameDrawer.setDrawingDisplacementPixels(-2f, 3f)
                    1, 3 -> gameDrawer.setDrawingDisplacementPixels(-2f, 2f)
                }
            }
        }

        gameDrawer.draw(shield, fighter.collidingEntity.x, fighter.collidingEntity.y, 0f,
                if (fighter.fighterEntityModule.facingDirection === TopDownEntityModule.Direction.LEFT) -1f else 1f,
                1f, 0f, 0f)
    }
}
