package com.dcostap.game.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.tools.texturepacker.TexturePacker
import com.dcostap.engine_2d.engine.Engine
import com.dcostap.game.ThisGame

import java.io.*
import java.util.Scanner

object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration()

        ThisGame.initStaticVars()

        config.width = Engine.BASE_WIDTH
        config.height = Engine.BASE_HEIGHT
        config.vSyncEnabled = false
        config.foregroundFPS = 60
        config.backgroundFPS = 60
        config.title = "libgdx"

        checkWhetherToPackImages()

        LwjglApplication(ThisGame(), config)
    }

    var atlasExportedImagesFolder = "atlas/textures_finished"
    var atlasImagesHashFile = "atlas/textureHash.txt"

    /** Will use texturePacker to pack all images, only if files have changed or atlas output files are not created
     * Loads settings from pack.json files in folders
     * Gradle task texturePacker does the same packing, Android launcher won't pack so use the task if needed  */
    fun checkWhetherToPackImages() {
        println("_____\nChecking image hashes...")
        var initTime = System.currentTimeMillis()

        val hashingTotal = hashAllFiles(File(atlasExportedImagesFolder))
        var c = 0
        try {
            val hashFile = File(atlasImagesHashFile)
            val atlasFile = File("atlas/atlas.atlas")
            val atlasImageFile = File("atlas/atlas.png")
            if (!hashFile.exists() || !atlasFile.exists() || !atlasImageFile.exists()) {
                val pw = PrintWriter(hashFile)
                pw.print(-1)
                pw.close()
            }
            val s = Scanner(hashFile)
            c = s.nextInt()
            if (hashingTotal != c) {
                val pw = PrintWriter(hashFile)
                pw.print(hashingTotal)
                pw.close()
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        println("Finished; time elapsed: " + java.lang.Double.toString((System.currentTimeMillis() - initTime) / 1000.toDouble()) + " s\n_____\n")

        if (hashingTotal != c) {
            initTime = System.currentTimeMillis()
            println("_____\nPacking images...")
            packImages(atlasExportedImagesFolder)
            println("Finished; time elapsed: " + java.lang.Double.toString((System.currentTimeMillis() - initTime) / 1000.toDouble()) + " s\n_____\n")
        }
    }

    private fun hashAllFiles(f: File): Int {
        var total = 0
        if (f.isDirectory) {
            for (content in f.listFiles()) {
                total += hashAllFiles(content)
            }
        } else {
            try {
                val input = FileInputStream(f.path)
                var c = 0
                while (c != -1) {
                    c = input.read()
                    total += c
                }
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        return total
    }

    private fun packImages(imagesFolder: String) {
        TexturePacker.process(imagesFolder, "atlas", "atlas")
    }
}

